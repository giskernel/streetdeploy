<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        return view('home')->with('users',$users);
    }

    public function edit(){

    }
    
    public function read(){
        
        $categories = new User;
        $data =  $categories->read();
    
        $res = array('success' => '1', 'result' => $data );
        return($res);

    }


    public function update(Request $request, $id)
    {
        $users = User::find($id);
        $users->role = $request->input('roleofuser');
        $users->categories = $request->input('catofuser');
        $users->save();
        return $users;
    }
}
