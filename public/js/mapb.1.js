mapboxgl.accessToken = 'pk.eyJ1IjoibW9vZGl0YXJzaG91YnkiLCJhIjoiY2p0d3RoeWl1MTV4azQ0cnRhd2owOTE5cyJ9.KTPQNpGdHIdLKHkleT2V6Q';
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mooditarshouby/ck182fn330gtl1cnjkeaa36x9',
    zoom: 10.5,
    center: [31.29, 29.98778571],
    pitch:20
});
var center = []
var lName;
var oldclicked;
var arr1=[];
var arr2=[];
var mainTypeColour = []
map.on ("load" , function (e){

map.addSource('tbl_data', {
type: 'geojson',
data: 'http://localhost:8080/geoserver/mapbox/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=mapbox%3Atbl_data&maxFeatures=42620&outputFormat=application%2Fjson'
});

map.addLayer({
'id': 'pointlayer',
'type': 'circle',
'source': 'tbl_data' ,
'layout': {},
'paint': {
"circle-radius": 2,
"circle-color": ['get', 'color']
}
});
     
//function namePlate(){
  map.on('click',function(e){
    var ff = map.queryRenderedFeatures(e.point);
    if ( ff.length != 0 ){
  var coordinates = [e.lngLat.lng,e.lngLat.lat]
  var name = ff[0].properties.Name;
  var addr = ff[0].properties.Full_Address;
  var cat = ff[0].properties.Category;
center=  [e.lngLat.lng,e.lngLat.lat];

   /*
  while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
  coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
  }
  */
  var Total_Reviews = ff[0].properties.Total_Reviews;
  var Photos = ff[0].properties.Photos;
  var Zip=  ff[0].properties.Zip;
  var Full_Address = ff[0].properties.Full_Address;
  var Main_Image_URL = ff[0].properties.Main_Image_URL;
  var City = ff[0].properties.City;
  <!-- var Final Conc = ff[0].properties.Final Conc; -->
  var Closed = ff[0].properties.Closed;
  var Virtual_Tour = ff[0].properties.Virtual_Tour;
  var Tuesday = ff[0].properties.Tuesday;
  var Phone = ff[0].properties.Phone;
  var Monday = ff[0].properties.Monday;
  var Sunday = ff[0].properties.Sunday;
  var State = ff[0].properties.State;
  var Plus_Code = ff[0].properties.Plus_Code;
  var Wednesday = ff[0].properties.Wednesday;
  var Street_Address = ff[0].properties.Street_Address;
  var Sub_Title = ff[0].properties.Sub_Title;
  var Saturday = ff[0].properties.Saturday;
  var Thursday = ff[0].properties.Thursday;
  var Type = ff[0].properties.Type;
  var Website = ff[0].properties.Website;
  var id = ff[0].properties.id;
  var Number = ff[0].properties.Number;
  var Claimed= ff[0].properties.Claimed;
  var Email = ff[0].properties.Email;
  var Rating = ff[0].properties.Rating;
  var Number = ff[0].properties.Number;
  var Friday = ff[0].properties.Friday;
  var Blockid = ff[0].properties.Block_id;
  var Housing = ff[0].properties.Housing_ty;
  var Income = ff[0].properties.Income;
  var arr1= [Total_Reviews,Photos,Zip,Full_Address,Main_Image_URL,City,Closed,Virtual_Tour,Tuesday,Phone,Monday,Sunday,State,Plus_Code,Wednesday,Street_Address,Sub_Title,Saturday,
  Thursday,Type,Website,id,Number,Claimed,Email,Rating,Number,Friday];
  
 
   
 // console.log("Hi",arr1[5]);
 function Popup(){
  new mapboxgl.Popup()
  .setLngLat(coordinates)
  .setHTML(str)
  .addTo(map);
 }
    }
  });

//}

  
  map.addControl(new MapboxGeocoder({
    accessToken: mapboxgl.accessToken,
    mapboxgl: mapboxgl
    }));

  map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
    enableHighAccuracy: true
    },
    trackUserLocation: true
    }));
   
    map.addControl(new mapboxgl.NavigationControl());

    map.addControl(new mapboxgl.ScaleControl());

  
function home(){
  map.setCenter([31.29, 29.98778571])
  map.setZoom(10.5)
}
