(function e(t, n, r) {
    function s(o, u) {
        if (!n[o]) {
            if (!t[o]) {
                const a = typeof require == "function" && require;
                if (!u && a) return a(o, !0);
                if (i) return i(o, !0);
                const f = new Error("Cannot find module '" + o + "'");
                throw f.code = "MODULE_NOT_FOUND", f
            }
            const l = n[o] = { exports: {} };
            t[o][0].call(l.exports, function (e) {
                const n = t[o][1][e];
                return s(n ? n : e)
            }, l, l.exports, e, t, n, r)
        }
        return n[o].exports
    }

    var i = typeof require == "function" && require;
    for (var o = 0; o < r.length; o++) s(r[o]);
    return s
})({
    1: [function (require, module, exports) {
        isochrone = require('./isochrone.js');
    }, { "./isochrone.js": 2 }],
    2: [function (require, module, exports) {
        const d3 = require('d3-request');
        const cheapRuler = require('cheap-ruler');
        const Conrec = require('./lib/conrec.js');

        const turf = {
            polygon: require('@turf/helpers').polygon,
            point: require('@turf/helpers').point,
            featureCollection: require('@turf/helpers').featureCollection,
            inside: require('@turf/inside')
        };

        function isochrone(startingPosition, parameters, cb) {
            // console.log("draw isochrone");
            //validate
            parameters = validate(startingPosition, parameters, cb);
            if (!parameters) return;

            startingPosition = startingPosition.map(function (coord) {
                return parseFloat(coord.toFixed(6))
            });

            const constants = {
                timeIncrement: 60,
                queryURL: {
                    'divergent': '?sources=0&destinations=all',
                },
                startingGrid: 2
            };

            const state = {
                travelTimes: {},
                lngs: {},
                lats: {},
                timeMaximum: typeof parameters.threshold === 'number' ? parameters.threshold : Math.max.apply(null, parameters.threshold)
            };

            state.thresholds = typeof parameters.threshold === 'number' ? listOutIntegers(state.timeMaximum, constants.timeIncrement) : parameters.threshold;
            state.lngs[startingPosition[0]] = 0;
            state.lats[startingPosition[1]] = 0;

            ruler = cheapRuler(startingPosition[1], 'kilometers');

            //track coords to request in each progressive round


            let outstandingRequests = 0;


            // kick off initial batch of queries
            extendBuffer([startingPosition], [constants.startingGrid]);

            function generateDiagonal(centerOffset, cellSize, dimensions) {

                const halfOffset = dimensions * 0.5 - 1;
                const output = [];


                for (let r = -halfOffset; r <= halfOffset + 1; r++) {

                    const xDelta = centerOffset[0] + r;
                    const yDelta = centerOffset[1] + r;

                    //first move left/right
                    const horizontalMovement = ruler.destination(startingPosition, xDelta * cellSize, 90);

                    //then move up/down
                    const verticalMovement =
                        ruler.destination(horizontalMovement, yDelta * cellSize, 0)
                            .map(function (coord) {
                                return parseFloat(coord.toFixed(6))
                            });

                    state.lngs[verticalMovement[0]] = xDelta;
                    state.lats[verticalMovement[1]] = yDelta;

                    output.push(verticalMovement)
                }

                return output
            }


            function generateBuffer(center, dimensions) {

                const centerIndex = [state.lngs[center[0]], state.lats[center[1]]];
                const diagonal = generateDiagonal(centerIndex, parameters.resolution * Math.pow(2, 0.5), dimensions);
                const grid = [];

                for (let r = 0; r < dimensions; r++) {
                    for (let c = 0; c < dimensions; c++) {
                        const lng = diagonal[r][0];
                        const lat = diagonal[c][1];
                        grid.push([lng, lat]);
                    }
                }

                return grid
            }

            // takes the points that need buffering, and buffer them
            function extendBuffer(toBuffer, radii) {

                const nextBatch = [];
                const timesSoFar = Object.keys(state.travelTimes).length;

                for (t in toBuffer) {
                    //generate buffer
                    const buffer = generateBuffer(toBuffer[t], radii[t]);

                    // dedupe buffer points and drop ones that are already sampled
                    buffer.forEach(function (pt) {

                        if (state.travelTimes[pt] || isNaN(pt[0])) return;
                        state.travelTimes[pt] = true;
                        nextBatch.push(pt)
                    })

                }
                batchRequests(nextBatch)
            }


            // route requests to smaller batches
            function batchRequests(coords) {
                const batchSize = parameters.batchSize - 1;
                outstandingRequests += Math.ceil(coords.length / batchSize);

                for (let c = 0; c < coords.length; c += batchSize) {
                    const batch = coords.slice(c, c + batchSize);
                    batch.unshift(startingPosition);
                    makeRequest(batch)
                }
            }


            // make API call, stows results in state.travelTimes, signals when all callbacks received

            function makeRequest(coords) {

                const formattedCoords = coords.map(function (coord, i) {
                    return [coord[0].toFixed(4), coord[1].toFixed(4)]
                }).join(';');

                const queryURL =
                    'https://api.mapbox.com/directions-matrix/v1/mapbox/' + parameters.mode + '/' + formattedCoords + constants.queryURL[parameters.direction] + '&access_token=' + parameters.token;
                d3.json(queryURL, function (err, resp) {

                    const parseDurations = {
                        'divergent': {
                            'data': resp.durations[0],
                            'timeObj': 'destinations'
                        },
                        'convergent': {
                            'data': resp.durations.map(function (item) {
                                return item[0]
                            }),
                            'timeObj': 'sources'
                        }
                    };

                    const durations = parseDurations[parameters.direction].data;
                    const toBuffer = [];
                    const bufferRadii = [];
                    const times = resp[parseDurations[parameters.direction].timeObj];

                    for (let i = 1; i < coords.length; i++) {

                        //calculate distance of grid coordinate from nearest neighbor on road, and assess penalty appropriately
                        const snapDistance = ruler.distance(times[i].location, coords[i]);
                        const snapPenalty = snapDistance > state.resolution / 2 ? state.timeMaximum : snapDistance * 1200;


                        // write time to record
                        const time = Math.ceil(parameters.fudgeFactor * durations[i] + snapPenalty);
                        state.travelTimes[coords[i]] = time;

                        // add to buffer list
                        const timeLeft = state.timeMaximum - time;
                        if (timeLeft > 0) {
                            toBuffer.push(coords[i]);
                            bufferRadii.push(calculateBufferRadius(timeLeft))
                        }
                    }
                    outstandingRequests--;

                    if (toBuffer.length > 0) extendBuffer(toBuffer, bufferRadii);

                    // when all callbacks received
                    else if (outstandingRequests === 0) polygonize()

                })
            }

            function calculateBufferRadius(timeRemaining) {
                return 4
            }

            function polygonize() {

                rawPoints = objectToArray(state.travelTimes, true);

                state.lngs = objectToArray(state.lngs, false).sort(function (a, b) {
                    return a - b
                });
                state.lats = objectToArray(state.lats, false).sort(function (a, b) {
                    return a - b
                }).reverse();

                conrec();

                function conrec() {

                    const points = [];

                    const twoDArray = [];

                    let c = new Conrec.Conrec;

                    for (r in state.lngs) {

                        const row = [];

                        for (d in state.lats) {
                            const coord = [state.lngs[r] - 0, state.lats[d]];
                            if (!state.travelTimes[coord]) state.travelTimes[coord] = [state.timeMaximum * 10];

                            const time = state.travelTimes[coord];

                            points.push(turf.point(coord, { time: time }));

                            row.push(time);
                        }
                        twoDArray.push(row)

                    }
                    postPoints = turf.featureCollection(points);

                    // build conrec
                    c.contour(twoDArray, 0, state.lngs.length - 1, 0, state.lats.length - 1, state.lngs, state.lats, state.thresholds.length, state.thresholds);

                    let contours = c.contourList();
                    polygons = [];

                    //iterate through contour hulls
                    for (c in contours) {

                        // get the current level
                        const level = contours[c].level;

                        //if no level, create it (reserve a first position in array for outer ring)
                        if (!polygons[level]) polygons[level] = [];

                        // create a shape
                        const shape = [];

                        // map x-y to lng,lat array
                        for (let k = 0; k < contours[c].length; k++) {
                            shape.push([contours[c][k].x, contours[c][k].y])
                        }


                        //close polygon loop
                        shape.push(shape[0]);

                        //make sure poly has at least 4 positions
                        for (var p = shape.length; p < 4; p++) {
                            shape.push(shape[0]);
                        }

                        //figure out if shape is an outer ring or an interior cavity, and slot it accordingly
                        if (turf.inside(turf.point(startingPosition), turf.polygon([shape])) === true) {
                            polygons[level][0] = shape;
                        } else {
                            polygons[level].push(shape)
                        }

                    }

                    if (parameters.keepIslands) {
                        contours = polygons.map(function (vertices, seconds) {
                            return turf.polygon(vertices, { time: seconds });
                        })
                            .filter(function (item) {
                                return item !== null
                            })
                    } else {

                        contours = polygons.map(function (vertices, seconds) {
                            //remove all holes that aren't actually holes, but polygons outside the main polygon
                            for (let p = vertices.length - 1; p > 0; p--) {
                                const ring = vertices[p];
                                let r = 0;
                                let soFarInside = true;
                                while (r < ring.length && soFarInside) {
                                    const pointIsInside = turf.inside(turf.point(ring[r]), turf.polygon([vertices[0]]));

                                    if (!pointIsInside) soFarInside = false;
                                    r++
                                }

                                if (!soFarInside) vertices.splice(p, 1)

                            }

                            const poly = vertices === null ? null : turf.polygon(vertices, {
                                time: seconds
                            });

                            return poly

                        })
                            .filter(function (item) {
                                return item !== null
                            })
                    }


                    hulls = turf.featureCollection(contours);
                    travelTimes = state.travelTimes;
                    cb(hulls)
                }

            }


            function objectToArray(object, arrayOfArrays) {

                const keys = Object.keys(object);

                if (!arrayOfArrays) return toNumbers(keys);
                const commaDelimitedNums = keys.map(function (coords) {
                    let commaDelimited = coords.split(',');

                    commaDelimited = toNumbers(commaDelimited);
                    return commaDelimited
                });

                return commaDelimitedNums
            }

            function toNumbers(strings) {
                return strings.map(
                    function (string) {
                        return parseFloat(string)
                    })
            }

            function listOutIntegers(max, increment) {
                const array = [];
                for (let v = increment; v <= max; v += increment) {
                    array.push(v)
                }
                return array
            }

            function validate(origin, parameters, cb) {

                const validator = {
                    token: { format: 'type', values: ['string'], required: true },
                    mode: {
                        format: 'among',
                        values: ['driving', 'cycling', 'walking'],
                        required: false,
                        default: 'driving'
                    },
                    direction: {
                        format: 'among',
                        values: ['divergent', 'convergent'],
                        required: false,
                        default: 'divergent'
                    },
                    threshold: { format: 'type', values: ['number', 'object'], required: true },
                    resolution: { format: 'range', min: 0.05, max: 2, required: false, default: 0.5 },
                    batchSize: { format: 'range', min: 2, max: Infinity, required: false, default: 25 },
                    fudgeFactor: { format: 'range', min: 0.5, max: 2, required: false, default: 1 },
                    keepIslands: { format: 'type', values: ['boolean'], required: false, default: false }
                };

                let error;

                // validate starting position
                if (!origin || typeof origin !== 'object' || origin.length !== 2) {
                    error = 'Starting position must be a longitude-latitude object, expressed as an array.'
                } else {
                    Object.keys(validator).forEach(function (key) {
                        const item = validator[key];

                        // make sure required parameters are present. if optional, fill in with default value
                        if (!parameters[key]) {
                            if (item.required) error = (key + ' required in query');
                            else parameters[key] = item.default
                        }

                        // ensure parameter is of right type
                        else if (item.format === 'type' && item.values.indexOf(typeof parameters[key]) === -1) {
                            error = (key + ' must be a ' + item.values.join(' or '))
                        }

                        //ensure parameter holds a valid value
                        else if (item.format === 'among' && item.values.indexOf(parameters[key]) === -1) {
                            error = (key + ' must be ' + item.values.join(' or '))
                        }

                        //ensure parameter falls within accepted range

                        else if (item.format === 'range') {
                            if (parameters[key] > item.max || parameters[key] < item.min) {
                                error = (key + ' must be between ' + item.min + ' and ' + item.max)
                            }
                        }

                        //special parsing for thresholds parameter
                        if (typeof parameters.threshold === 'object') {
                            if (!parameters.threshold.length || !parameters.threshold.every(function (item) {
                                return typeof item === 'number'
                            })) {
                                error = ('thresholds must be an array of numbers')
                            }
                        }
                    });
                }


                if (error) {
                    throw new Error(error);
                    return cb(new Error(error))
                } else return parameters
            }
        }


        module.exports = exports = isochrone;

    }, { "./lib/conrec.js": 3, "@turf/helpers": 4, "@turf/inside": 5, "cheap-ruler": 7, "d3-request": 11 }],
    3: [function (require, module, exports) {


        (function (exports) {
            exports.Conrec = Conrec;

            const EPSILON = 1e-10;

            function pointsEqual(a, b) {
                const x = a.x - b.x, y = a.y - b.y;
                return x * x + y * y < EPSILON;
            }

            function reverseList(list) {
                let pp = list.head;

                while (pp) {
                    // swap prev/next pointers
                    var temp = pp.next;
                    pp.next = pp.prev;
                    pp.prev = temp;

                    // continue through the list
                    pp = temp;
                }

                // swap head/tail pointers
                var temp = list.head;
                list.head = list.tail;
                list.tail = temp;
            }

            function ContourBuilder(level) {
                this.level = level;
                this.s = null;
                this.count = 0;
            }

            ContourBuilder.prototype.remove_seq = function (list) {
                // if list is the first item, static ptr s is updated
                if (list.prev) {
                    list.prev.next = list.next;
                } else {
                    this.s = list.next;
                }

                if (list.next) {
                    list.next.prev = list.prev;
                }
                --this.count;
            };
            ContourBuilder.prototype.addSegment = function (a, b) {
                let ss = this.s;
                let ma = null;
                let mb = null;
                let prependA = false;
                let prependB = false;

                while (ss) {
                    if (ma == null) {
                        // no match for a yet
                        if (pointsEqual(a, ss.head.p)) {
                            ma = ss;
                            prependA = true;
                        } else if (pointsEqual(a, ss.tail.p)) {
                            ma = ss;
                        }
                    }
                    if (mb == null) {
                        // no match for b yet
                        if (pointsEqual(b, ss.head.p)) {
                            mb = ss;
                            prependB = true;
                        } else if (pointsEqual(b, ss.tail.p)) {
                            mb = ss;
                        }
                    }
                    // if we matched both no need to continue searching
                    if (mb != null && ma != null) {
                        break;
                    } else {
                        ss = ss.next;
                    }
                }

                // c is the case selector based on which of ma and/or mb are set
                const c = ((ma != null) ? 1 : 0) | ((mb != null) ? 2 : 0);

                switch (c) {
                    case 0:   // both unmatched, add as new sequence
                        const aa = { p: a, prev: null };
                        const bb = { p: b, next: null };
                        aa.next = bb;
                        bb.prev = aa;

                        // create sequence element and push onto head of main list. The order
                        // of items in this list is unimportant
                        ma = { head: aa, tail: bb, next: this.s, prev: null, closed: false };
                        if (this.s) {
                            this.s.prev = ma;
                        }
                        this.s = ma;

                        ++this.count;    // not essential - tracks number of unmerged sequences
                        break;

                    case 1:   // a matched, b did not - thus b extends sequence ma
                        var pp = { p: b };

                        if (prependA) {
                            pp.next = ma.head;
                            pp.prev = null;
                            ma.head.prev = pp;
                            ma.head = pp;
                        } else {
                            pp.next = null;
                            pp.prev = ma.tail;
                            ma.tail.next = pp;
                            ma.tail = pp;
                        }
                        break;

                    case 2:   // b matched, a did not - thus a extends sequence mb
                        var pp = { p: a };

                        if (prependB) {
                            pp.next = mb.head;
                            pp.prev = null;
                            mb.head.prev = pp;
                            mb.head = pp;
                        } else {
                            pp.next = null;
                            pp.prev = mb.tail;
                            mb.tail.next = pp;
                            mb.tail = pp;
                        }
                        break;

                    case 3:   // both matched, can merge sequences
                        // if the sequences are the same, do nothing, as we are simply closing this path (could set a flag)

                        if (ma === mb) {
                            var pp = { p: ma.tail.p, next: ma.head, prev: null };
                            ma.head.prev = pp;
                            ma.head = pp;
                            ma.closed = true;
                            break;
                        }

                        // there are 4 ways the sequence pair can be joined. The current setting of prependA and
                        // prependB will tell us which type of join is needed. For head/head and tail/tail joins
                        // one sequence needs to be reversed
                        switch ((prependA ? 1 : 0) | (prependB ? 2 : 0)) {
                            case 0:   // tail-tail
                                // reverse ma and append to mb
                                reverseList(ma);
                            // fall through to head/tail case
                            case 1:   // head-tail
                                // ma is appended to mb and ma discarded
                                mb.tail.next = ma.head;
                                ma.head.prev = mb.tail;
                                mb.tail = ma.tail;

                                //discard ma sequence record
                                this.remove_seq(ma);
                                break;

                            case 3:   // head-head
                                // reverse ma and append mb to it
                                reverseList(ma);
                            // fall through to tail/head case
                            case 2:   // tail-head
                                // mb is appended to ma and mb is discarded
                                ma.tail.next = mb.head;
                                mb.head.prev = ma.tail;
                                ma.tail = mb.tail;

                                //discard mb sequence record
                                this.remove_seq(mb);
                                break;
                        }
                }
            };

            /**
             * Implements CONREC.
             *
             * @param {function} drawContour function for drawing contour.  Defaults to a
             *                               custom "contour builder", which populates the
             *                               contours property.
             */
            function Conrec(drawContour) {
                // console.log("draw contour");
                if (!drawContour) {
                    const c = this;
                    c.contours = {};
                    /**
                     * drawContour - interface for implementing the user supplied method to
                     * render the countours.
                     *
                     * Draws a line between the start and end coordinates.
                     *
                     * @param startX    - start coordinate for X
                     * @param startY    - start coordinate for Y
                     * @param endX      - end coordinate for X
                     * @param endY      - end coordinate for Y
                     * @param contourLevel - Contour level for line.
                     */
                    this.drawContour = function (startX, startY, endX, endY, contourLevel, k) {
                        let cb = c.contours[k];
                        if (!cb) {
                            cb = c.contours[k] = new ContourBuilder(contourLevel);
                        }
                        cb.addSegment({ x: startX, y: startY }, { x: endX, y: endY });
                    };
                    this.contourList = function () {
                        const l = [];
                        const a = c.contours;
                        for (let k in a) {
                            let s = a[k].s;
                            const level = a[k].level;
                            while (s) {
                                let h = s.head;
                                const l2 = [];
                                l2.level = level;
                                l2.k = k;
                                while (h && h.p) {
                                    l2.push(h.p);
                                    h = h.next;
                                }
                                l.push(l2);
                                s = s.next;
                            }
                        }
                        l.sort(function (a, b) {
                            return a.k - b.k
                        });
                        return l;
                    }
                } else {
                    this.drawContour = drawContour;
                }
                this.h = new Array(5);
                this.sh = new Array(5);
                this.xh = new Array(5);
                this.yh = new Array(5);
            }

            /**
             * contour is a contouring subroutine for rectangularily spaced data
             *
             * It emits calls to a line drawing subroutine supplied by the user which
             * draws a contour map corresponding to real*4data on a randomly spaced
             * rectangular grid. The coordinates emitted are in the same units given in
             * the x() and y() arrays.
             *
             * Any number of contour levels may be specified but they must be in order of
             * increasing value.
             *
             *
             * @param {number[][]} d - matrix of data to contour
             * @param {number} ilb,iub,jlb,jub - index bounds of data matrix
             *
             *             The following two, one dimensional arrays (x and y) contain
             *             the horizontal and vertical coordinates of each sample points.
             * @param {number[]} x  - data matrix column coordinates
             * @param {number[]} y  - data matrix row coordinates
             * @param {number} nc   - number of contour levels
             * @param {number[]} z  - contour levels in increasing order.
             */
            Conrec.prototype.contour = function (d, ilb, iub, jlb, jub, x, y, nc, z) {
                const h = this.h, sh = this.sh, xh = this.xh, yh = this.yh;
                const drawContour = this.drawContour;
                this.contours = {};

                /** private */
                const xsect = function (p1, p2) {
                    return (h[p2] * xh[p1] - h[p1] * xh[p2]) / (h[p2] - h[p1]);
                };

                const ysect = function (p1, p2) {
                    return (h[p2] * yh[p1] - h[p1] * yh[p2]) / (h[p2] - h[p1]);
                };
                let m1;
                let m2;
                let m3;
                let case_value;
                let dmin;
                let dmax;
                let x1 = 0.0;
                let x2 = 0.0;
                let y1 = 0.0;
                let y2 = 0.0;

                // The indexing of im and jm should be noted as it has to start from zero
                // unlike the fortran counter part
                const im = [0, 1, 1, 0];
                const jm = [0, 0, 1, 1];

                // Note that castab is arranged differently from the FORTRAN code because
                // Fortran and C/C++ arrays are transposed of each other, in this case
                // it is more tricky as castab is in 3 dimensions
                const castab = [
                    [
                        [0, 0, 8], [0, 2, 5], [7, 6, 9]
                    ],
                    [
                        [0, 3, 4], [1, 3, 1], [4, 3, 0]
                    ],
                    [
                        [9, 6, 7], [5, 2, 0], [8, 0, 0]
                    ]
                ];

                for (let j = (jub - 1); j >= jlb; j--) {
                    for (let i = ilb; i <= iub - 1; i++) {
                        let temp1, temp2;
                        temp1 = Math.min(d[i][j], d[i][j + 1]);
                        temp2 = Math.min(d[i + 1][j], d[i + 1][j + 1]);
                        dmin = Math.min(temp1, temp2);
                        temp1 = Math.max(d[i][j], d[i][j + 1]);
                        temp2 = Math.max(d[i + 1][j], d[i + 1][j + 1]);
                        dmax = Math.max(temp1, temp2);

                        if (dmax >= z[0] && dmin <= z[nc - 1]) {
                            for (let k = 0; k < nc; k++) {
                                if (z[k] >= dmin && z[k] <= dmax) {
                                    for (var m = 4; m >= 0; m--) {
                                        if (m > 0) {
                                            // The indexing of im and jm should be noted as it has to
                                            // start from zero
                                            h[m] = d[i + im[m - 1]][j + jm[m - 1]] - z[k];
                                            xh[m] = x[i + im[m - 1]];
                                            yh[m] = y[j + jm[m - 1]];
                                        } else {
                                            h[0] = 0.25 * (h[1] + h[2] + h[3] + h[4]);
                                            xh[0] = 0.5 * (x[i] + x[i + 1]);
                                            yh[0] = 0.5 * (y[j] + y[j + 1]);
                                        }
                                        if (h[m] > EPSILON) {
                                            sh[m] = 1;
                                        } else if (h[m] < -EPSILON) {
                                            sh[m] = -1;
                                        } else
                                            sh[m] = 0;
                                    }
                                    //
                                    // Note: at this stage the relative heights of the corners and the
                                    // centre are in the h array, and the corresponding coordinates are
                                    // in the xh and yh arrays. The centre of the box is indexed by 0
                                    // and the 4 corners by 1 to 4 as shown below.
                                    // Each triangle is then indexed by the parameter m, and the 3
                                    // vertices of each triangle are indexed by parameters m1,m2,and
                                    // m3.
                                    // It is assumed that the centre of the box is always vertex 2
                                    // though this isimportant only when all 3 vertices lie exactly on
                                    // the same contour level, in which case only the side of the box
                                    // is drawn.
                                    //
                                    //
                                    //      vertex 4 +-------------------+ vertex 3
                                    //               | \               / |
                                    //               |   \    m-3    /   |
                                    //               |     \       /     |
                                    //               |       \   /       |
                                    //               |  m=2    X   m=2   |       the centre is vertex 0
                                    //               |       /   \       |
                                    //               |     /       \     |
                                    //               |   /    m=1    \   |
                                    //               | /               \ |
                                    //      vertex 1 +-------------------+ vertex 2
                                    //
                                    //
                                    //
                                    //               Scan each triangle in the box
                                    //
                                    for (m = 1; m <= 4; m++) {
                                        m1 = m;
                                        m2 = 0;
                                        if (m != 4) {
                                            m3 = m + 1;
                                        } else {
                                            m3 = 1;
                                        }
                                        case_value = castab[sh[m1] + 1][sh[m2] + 1][sh[m3] + 1];
                                        if (case_value != 0) {
                                            switch (case_value) {
                                                case 1: // Line between vertices 1 and 2
                                                    x1 = xh[m1];
                                                    y1 = yh[m1];
                                                    x2 = xh[m2];
                                                    y2 = yh[m2];
                                                    break;
                                                case 2: // Line between vertices 2 and 3
                                                    x1 = xh[m2];
                                                    y1 = yh[m2];
                                                    x2 = xh[m3];
                                                    y2 = yh[m3];
                                                    break;
                                                case 3: // Line between vertices 3 and 1
                                                    x1 = xh[m3];
                                                    y1 = yh[m3];
                                                    x2 = xh[m1];
                                                    y2 = yh[m1];
                                                    break;
                                                case 4: // Line between vertex 1 and side 2-3
                                                    x1 = xh[m1];
                                                    y1 = yh[m1];
                                                    x2 = xsect(m2, m3);
                                                    y2 = ysect(m2, m3);
                                                    break;
                                                case 5: // Line between vertex 2 and side 3-1
                                                    x1 = xh[m2];
                                                    y1 = yh[m2];
                                                    x2 = xsect(m3, m1);
                                                    y2 = ysect(m3, m1);
                                                    break;
                                                case 6: //  Line between vertex 3 and side 1-2
                                                    x1 = xh[m3];
                                                    y1 = yh[m3];
                                                    x2 = xsect(m1, m2);
                                                    y2 = ysect(m1, m2);
                                                    break;
                                                case 7: // Line between sides 1-2 and 2-3
                                                    x1 = xsect(m1, m2);
                                                    y1 = ysect(m1, m2);
                                                    x2 = xsect(m2, m3);
                                                    y2 = ysect(m2, m3);
                                                    break;
                                                case 8: // Line between sides 2-3 and 3-1
                                                    x1 = xsect(m2, m3);
                                                    y1 = ysect(m2, m3);
                                                    x2 = xsect(m3, m1);
                                                    y2 = ysect(m3, m1);
                                                    break;
                                                case 9: // Line between sides 3-1 and 1-2
                                                    x1 = xsect(m3, m1);
                                                    y1 = ysect(m3, m1);
                                                    x2 = xsect(m1, m2);
                                                    y2 = ysect(m1, m2);
                                                    break;
                                                default:
                                                    break;
                                            }
                                            // Put your processing code here and comment out the printf
                                            //printf("%f %f %f %f %f\n",x1,y1,x2,y2,z[k]);
                                            drawContour(x1, y1, x2, y2, z[k], k);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })(typeof exports !== "undefined" ? exports : window);

    }, {}],
    4: [function (require, module, exports) {
        /**
         * Wraps a GeoJSON {@link Geometry} in a GeoJSON {@link Feature}.
         *
         * @name feature
         * @param {Geometry} geometry input geometry
         * @param {Object} properties properties
         * @returns {Feature} a GeoJSON Feature
         * @example
         * var geometry = {
         *   "type": "Point",
         *   "coordinates": [110, 50]
         * };
         *
         * var feature = turf.feature(geometry);
         *
         * //=feature
         */
        function feature(geometry, properties) {
            if (!geometry) throw new Error('No geometry passed');

            return {
                type: 'Feature',
                properties: properties || {},
                geometry: geometry
            };
        }

        /**
         * Takes coordinates and properties (optional) and returns a new {@link Point} feature.
         *
         * @name point
         * @param {Array<number>} coordinates longitude, latitude position (each in decimal degrees)
         * @param {Object=} properties an Object that is used as the {@link Feature}'s
         * properties
         * @returns {Feature<Point>} a Point feature
         * @example
         * var point = turf.point([-75.343, 39.984]);
         *
         * //=point
         */
        function point(coordinates, properties) {
            if (!coordinates) throw new Error('No coordinates passed');
            if (coordinates.length === undefined) throw new Error('Coordinates must be an array');
            if (coordinates.length < 2) throw new Error('Coordinates must be at least 2 numbers long');
            if (typeof coordinates[0] !== 'number' || typeof coordinates[1] !== 'number') throw new Error('Coordinates must numbers');

            return feature({
                type: 'Point',
                coordinates: coordinates
            }, properties);
        }

        /**
         * Takes an array of LinearRings and optionally an {@link Object} with properties and returns a {@link Polygon} feature.
         *
         * @name polygon
         * @param {Array<Array<Array<number>>>} coordinates an array of LinearRings
         * @param {Object=} properties a properties object
         * @returns {Feature<Polygon>} a Polygon feature
         * @throws {Error} throw an error if a LinearRing of the polygon has too few positions
         * or if a LinearRing of the Polygon does not have matching Positions at the beginning & end.
         * @example
         * var polygon = turf.polygon([[
         *   [-2.275543, 53.464547],
         *   [-2.275543, 53.489271],
         *   [-2.215118, 53.489271],
         *   [-2.215118, 53.464547],
         *   [-2.275543, 53.464547]
         * ]], { name: 'poly1', population: 400});
         *
         * //=polygon
         */
        function polygon(coordinates, properties) {
            if (!coordinates) throw new Error('No coordinates passed');

            for (let i = 0; i < coordinates.length; i++) {
                const ring = coordinates[i];
                if (ring.length < 4) {
                    throw new Error('Each LinearRing of a Polygon must have 4 or more Positions.');
                }
                for (let j = 0; j < ring[ring.length - 1].length; j++) {
                    if (ring[ring.length - 1][j] !== ring[0][j]) {
                        throw new Error('First and last Position are not equivalent.');
                    }
                }
            }

            return feature({
                type: 'Polygon',
                coordinates: coordinates
            }, properties);
        }

        /**
         * Creates a {@link LineString} based on a
         * coordinate array. Properties can be added optionally.
         *
         * @name lineString
         * @param {Array<Array<number>>} coordinates an array of Positions
         * @param {Object=} properties an Object of key-value pairs to add as properties
         * @returns {Feature<LineString>} a LineString feature
         * @throws {Error} if no coordinates are passed
         * @example
         * var linestring1 = turf.lineString([
         *   [-21.964416, 64.148203],
         *   [-21.956176, 64.141316],
         *   [-21.93901, 64.135924],
         *   [-21.927337, 64.136673]
         * ]);
         * var linestring2 = turf.lineString([
         *   [-21.929054, 64.127985],
         *   [-21.912918, 64.134726],
         *   [-21.916007, 64.141016],
         *   [-21.930084, 64.14446]
         * ], {name: 'line 1', distance: 145});
         *
         * //=linestring1
         *
         * //=linestring2
         */
        function lineString(coordinates, properties) {
            if (!coordinates) throw new Error('No coordinates passed');
            if (coordinates.length < 2) throw new Error('Coordinates must be an array of two or more positions');

            return feature({
                type: 'LineString',
                coordinates: coordinates
            }, properties);
        }

        /**
         * Takes one or more {@link Feature|Features} and creates a {@link FeatureCollection}.
         *
         * @name featureCollection
         * @param {Feature[]} features input features
         * @returns {FeatureCollection} a FeatureCollection of input features
         * @example
         * var features = [
         *  turf.point([-75.343, 39.984], {name: 'Location A'}),
         *  turf.point([-75.833, 39.284], {name: 'Location B'}),
         *  turf.point([-75.534, 39.123], {name: 'Location C'})
         * ];
         *
         * var collection = turf.featureCollection(features);
         *
         * //=collection
         */
        function featureCollection(features) {
            if (!features) throw new Error('No features passed');

            return {
                type: 'FeatureCollection',
                features: features
            };
        }

        /**
         * Creates a {@link Feature<MultiLineString>} based on a
         * coordinate array. Properties can be added optionally.
         *
         * @name multiLineString
         * @param {Array<Array<Array<number>>>} coordinates an array of LineStrings
         * @param {Object=} properties an Object of key-value pairs to add as properties
         * @returns {Feature<MultiLineString>} a MultiLineString feature
         * @throws {Error} if no coordinates are passed
         * @example
         * var multiLine = turf.multiLineString([[[0,0],[10,10]]]);
         *
         * //=multiLine
         */
        function multiLineString(coordinates, properties) {
            if (!coordinates) throw new Error('No coordinates passed');

            return feature({
                type: 'MultiLineString',
                coordinates: coordinates
            }, properties);
        }

        /**
         * Creates a {@link Feature<MultiPoint>} based on a
         * coordinate array. Properties can be added optionally.
         *
         * @name multiPoint
         * @param {Array<Array<number>>} coordinates an array of Positions
         * @param {Object=} properties an Object of key-value pairs to add as properties
         * @returns {Feature<MultiPoint>} a MultiPoint feature
         * @throws {Error} if no coordinates are passed
         * @example
         * var multiPt = turf.multiPoint([[0,0],[10,10]]);
         *
         * //=multiPt
         */
        function multiPoint(coordinates, properties) {
            if (!coordinates) throw new Error('No coordinates passed');

            return feature({
                type: 'MultiPoint',
                coordinates: coordinates
            }, properties);
        }

        /**
         * Creates a {@link Feature<MultiPolygon>} based on a
         * coordinate array. Properties can be added optionally.
         *
         * @name multiPolygon
         * @param {Array<Array<Array<Array<number>>>>} coordinates an array of Polygons
         * @param {Object=} properties an Object of key-value pairs to add as properties
         * @returns {Feature<MultiPolygon>} a multipolygon feature
         * @throws {Error} if no coordinates are passed
         * @example
         * var multiPoly = turf.multiPolygon([[[[0,0],[0,10],[10,10],[10,0],[0,0]]]]);
         *
         * //=multiPoly
         *
         */
        function multiPolygon(coordinates, properties) {
            if (!coordinates) throw new Error('No coordinates passed');

            return feature({
                type: 'MultiPolygon',
                coordinates: coordinates
            }, properties);
        }

        /**
         * Creates a {@link Feature<GeometryCollection>} based on a
         * coordinate array. Properties can be added optionally.
         *
         * @name geometryCollection
         * @param {Array<{Geometry}>} geometries an array of GeoJSON Geometries
         * @param {Object=} properties an Object of key-value pairs to add as properties
         * @returns {Feature<GeometryCollection>} a GeoJSON GeometryCollection Feature
         * @example
         * var pt = {
         *     "type": "Point",
         *       "coordinates": [100, 0]
         *     };
         * var line = {
         *     "type": "LineString",
         *     "coordinates": [ [101, 0], [102, 1] ]
         *   };
         * var collection = turf.geometryCollection([pt, line]);
         *
         * //=collection
         */
        function geometryCollection(geometries, properties) {
            if (!geometries) throw new Error('geometries is required');

            return feature({
                type: 'GeometryCollection',
                geometries: geometries
            }, properties);
        }

        // https://en.wikipedia.org/wiki/Great-circle_distance#Radius_for_spherical_Earth
        const factors = {
            miles: 3960,
            nauticalmiles: 3441.145,
            degrees: 57.2957795,
            radians: 1,
            inches: 250905600,
            yards: 6969600,
            meters: 6373000,
            metres: 6373000,
            centimeters: 6.373e+8,
            centimetres: 6.373e+8,
            kilometers: 6373,
            kilometres: 6373,
            feet: 20908792.65
        };

        /**
         * Round number to precision
         *
         * @param {number} num Number
         * @param {number} [precision=0] Precision
         * @returns {number} rounded number
         * @example
         * round(120.4321)
         * //=120
         *
         * round(120.4321, 2)
         * //=120.43
         */
        function round(num, precision) {
            if (num === undefined || num === null || isNaN(num)) throw new Error('num is required');
            if (precision && !(precision >= 0)) throw new Error('precision must be a positive number');
            const multiplier = Math.pow(10, precision || 0);
            return Math.round(num * multiplier) / multiplier;
        }

        /**
         * Convert a distance measurement (assuming a spherical Earth) from radians to a more friendly unit.
         * Valid units: miles, nauticalmiles, inches, yards, meters, metres, kilometers, centimeters, feet
         *
         * @name radiansToDistance
         * @param {number} radians in radians across the sphere
         * @param {string} [units=kilometers] can be degrees, radians, miles, or kilometers inches, yards, metres, meters, kilometres, kilometers.
         * @returns {number} distance
         */
        function radiansToDistance(radians, units) {
            if (radians === undefined || radians === null) throw new Error('radians is required');

            const factor = factors[units || 'kilometers'];
            if (!factor) throw new Error('units is invalid');
            return radians * factor;
        }

        /**
         * Convert a distance measurement (assuming a spherical Earth) from a real-world unit into radians
         * Valid units: miles, nauticalmiles, inches, yards, meters, metres, kilometers, centimeters, feet
         *
         * @name distanceToRadians
         * @param {number} distance in real units
         * @param {string} [units=kilometers] can be degrees, radians, miles, or kilometers inches, yards, metres, meters, kilometres, kilometers.
         * @returns {number} radians
         */
        function distanceToRadians(distance, units) {
            if (distance === undefined || distance === null) throw new Error('distance is required');

            const factor = factors[units || 'kilometers'];
            if (!factor) throw new Error('units is invalid');
            return distance / factor;
        }

        /**
         * Convert a distance measurement (assuming a spherical Earth) from a real-world unit into degrees
         * Valid units: miles, nauticalmiles, inches, yards, meters, metres, centimeters, kilometres, feet
         *
         * @name distanceToDegrees
         * @param {number} distance in real units
         * @param {string} [units=kilometers] can be degrees, radians, miles, or kilometers inches, yards, metres, meters, kilometres, kilometers.
         * @returns {number} degrees
         */
        function distanceToDegrees(distance, units) {
            return radians2degrees(distanceToRadians(distance, units));
        }

        /**
         * Converts any bearing angle from the north line direction (positive clockwise)
         * and returns an angle between 0-360 degrees (positive clockwise), 0 being the north line
         *
         * @name bearingToAngle
         * @param {number} bearing angle, between -180 and +180 degrees
         * @returns {number} angle between 0 and 360 degrees
         */
        function bearingToAngle(bearing) {
            if (bearing === null || bearing === undefined) throw new Error('bearing is required');

            let angle = bearing % 360;
            if (angle < 0) angle += 360;
            return angle;
        }

        /**
         * Converts an angle in radians to degrees
         *
         * @name radians2degrees
         * @param {number} radians angle in radians
         * @returns {number} degrees between 0 and 360 degrees
         */
        function radians2degrees(radians) {
            if (radians === null || radians === undefined) throw new Error('radians is required');

            const degrees = radians % (2 * Math.PI);
            return degrees * 180 / Math.PI;
        }

        /**
         * Converts an angle in degrees to radians
         *
         * @name degrees2radians
         * @param {number} degrees angle between 0 and 360 degrees
         * @returns {number} angle in radians
         */
        function degrees2radians(degrees) {
            if (degrees === null || degrees === undefined) throw new Error('degrees is required');

            const radians = degrees % 360;
            return radians * Math.PI / 180;
        }


        /**
         * Converts a distance to the requested unit.
         * Valid units: miles, nauticalmiles, inches, yards, meters, metres, kilometers, centimeters, feet
         *
         * @param {number} distance to be converted
         * @param {string} originalUnit of the distance
         * @param {string} [finalUnit=kilometers] returned unit
         * @returns {number} the converted distance
         */
        function convertDistance(distance, originalUnit, finalUnit) {
            if (distance === null || distance === undefined) throw new Error('distance is required');
            if (!(distance >= 0)) throw new Error('distance must be a positive number');

            const convertedDistance = radiansToDistance(distanceToRadians(distance, originalUnit), finalUnit || 'kilometers');
            return convertedDistance;
        }


        module.exports = {
            feature: feature,
            featureCollection: featureCollection,
            geometryCollection: geometryCollection,
            point: point,
            multiPoint: multiPoint,
            lineString: lineString,
            multiLineString: multiLineString,
            polygon: polygon,
            multiPolygon: multiPolygon,
            radiansToDistance: radiansToDistance,
            distanceToRadians: distanceToRadians,
            distanceToDegrees: distanceToDegrees,
            radians2degrees: radians2degrees,
            degrees2radians: degrees2radians,
            bearingToAngle: bearingToAngle,
            convertDistance: convertDistance,
            round: round
        };

    }, {}],
    5: [function (require, module, exports) {
        const invariant = require('@turf/invariant');

        // http://en.wikipedia.org/wiki/Even%E2%80%93odd_rule
        // modified from: https://github.com/substack/point-in-polygon/blob/master/index.js
        // which was modified from http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

        /**
         * Takes a {@link Point} and a {@link Polygon} or {@link MultiPolygon} and determines if the point resides inside the polygon. The polygon can
         * be convex or concave. The function accounts for holes.
         *
         * @name inside
         * @param {Feature<Point>} point input point
         * @param {Feature<(Polygon|MultiPolygon)>} polygon input polygon or multipolygon
         * @returns {boolean} `true` if the Point is inside the Polygon; `false` if the Point is not inside the Polygon
         * @example
         * var pt = turf.point([-77, 44]);
         * var poly = turf.polygon([[
         *   [-81, 41],
         *   [-81, 47],
         *   [-72, 47],
         *   [-72, 41],
         *   [-81, 41]
         * ]]);
         *
         * var isInside = turf.inside(pt, poly);
         *
         * //=isInside
         */
        module.exports = function (point, polygon) {
            const pt = invariant.getCoord(point);
            let polys = polygon.geometry.coordinates;
            // normalize to multipolygon
            if (polygon.geometry.type === 'Polygon') polys = [polys];

            for (var i = 0, insidePoly = false; i < polys.length && !insidePoly; i++) {
                // check if it is in the outer ring first
                if (inRing(pt, polys[i][0])) {
                    let inHole = false;
                    let k = 1;
                    // check for the point in any of the holes
                    while (k < polys[i].length && !inHole) {
                        if (inRing(pt, polys[i][k], true)) {
                            inHole = true;
                        }
                        k++;
                    }
                    if (!inHole) insidePoly = true;
                }
            }
            return insidePoly;
        };

        // pt is [x,y] and ring is [[x,y], [x,y],..]
        function inRing(pt, ring, ignoreBoundary) {
            let isInside = false;
            if (ring[0][0] === ring[ring.length - 1][0] && ring[0][1] === ring[ring.length - 1][1]) ring = ring.slice(0, ring.length - 1);

            let i = 0, j = ring.length - 1;
            for (; i < ring.length; j = i++) {
                const xi = ring[i][0], yi = ring[i][1];
                const xj = ring[j][0], yj = ring[j][1];
                const onBoundary = (pt[1] * (xi - xj) + yi * (xj - pt[0]) + yj * (pt[0] - xi) === 0) &&
                    ((xi - pt[0]) * (xj - pt[0]) <= 0) && ((yi - pt[1]) * (yj - pt[1]) <= 0);
                if (onBoundary) return !ignoreBoundary;
                const intersect = ((yi > pt[1]) !== (yj > pt[1])) &&
                    (pt[0] < (xj - xi) * (pt[1] - yi) / (yj - yi) + xi);
                if (intersect) isInside = !isInside;
            }
            return isInside;
        }

    }, { "@turf/invariant": 6 }],
    6: [function (require, module, exports) {
        /**
         * Unwrap a coordinate from a Point Feature, Geometry or a single coordinate.
         *
         * @param {Array<any>|Geometry|Feature<Point>} obj any value
         * @returns {Array<number>} coordinates
         */
        function getCoord(obj) {
            if (!obj) throw new Error('No obj passed');

            const coordinates = getCoords(obj);

            // getCoord() must contain at least two numbers (Point)
            if (coordinates.length > 1 &&
                typeof coordinates[0] === 'number' &&
                typeof coordinates[1] === 'number') {
                return coordinates;
            } else {
                throw new Error('Coordinate is not a valid Point');
            }
        }

        /**
         * Unwrap coordinates from a Feature, Geometry Object or an Array of numbers
         *
         * @param {Array<any>|Geometry|Feature<any>} obj any value
         * @returns {Array<any>} coordinates
         */
        function getCoords(obj) {
            if (!obj) throw new Error('No obj passed');
            let coordinates;

            // Array of numbers
            if (obj.length) {
                coordinates = obj;

                // Geometry Object
            } else if (obj.coordinates) {
                coordinates = obj.coordinates;

                // Feature
            } else if (obj.geometry && obj.geometry.coordinates) {
                coordinates = obj.geometry.coordinates;
            }
            // Checks if coordinates contains a number
            if (coordinates) {
                containsNumber(coordinates);
                return coordinates;
            }
            throw new Error('No valid coordinates');
        }

        /**
         * Checks if coordinates contains a number
         *
         * @private
         * @param {Array<any>} coordinates GeoJSON Coordinates
         * @returns {boolean} true if Array contains a number
         */
        function containsNumber(coordinates) {
            if (coordinates.length > 1 &&
                typeof coordinates[0] === 'number' &&
                typeof coordinates[1] === 'number') {
                return true;
            }
            if (coordinates[0].length) {
                return containsNumber(coordinates[0]);
            }
            throw new Error('coordinates must only contain numbers');
        }

        /**
         * Enforce expectations about types of GeoJSON objects for Turf.
         *
         * @alias geojsonType
         * @param {GeoJSON} value any GeoJSON object
         * @param {string} type expected GeoJSON type
         * @param {string} name name of calling function
         * @throws {Error} if value is not the expected type.
         */
        function geojsonType(value, type, name) {
            if (!type || !name) throw new Error('type and name required');

            if (!value || value.type !== type) {
                throw new Error('Invalid input to ' + name + ': must be a ' + type + ', given ' + value.type);
            }
        }

        /**
         * Enforce expectations about types of {@link Feature} inputs for Turf.
         * Internally this uses {@link geojsonType} to judge geometry types.
         *
         * @alias featureOf
         * @param {Feature} feature a feature with an expected geometry type
         * @param {string} type expected GeoJSON type
         * @param {string} name name of calling function
         * @throws {Error} error if value is not the expected type.
         */
        function featureOf(feature, type, name) {
            if (!feature) throw new Error('No feature passed');
            if (!name) throw new Error('.featureOf() requires a name');
            if (!feature || feature.type !== 'Feature' || !feature.geometry) {
                throw new Error('Invalid input to ' + name + ', Feature with geometry required');
            }
            if (!feature.geometry || feature.geometry.type !== type) {
                throw new Error('Invalid input to ' + name + ': must be a ' + type + ', given ' + feature.geometry.type);
            }
        }

        /**
         * Enforce expectations about types of {@link FeatureCollection} inputs for Turf.
         * Internally this uses {@link geojsonType} to judge geometry types.
         *
         * @alias collectionOf
         * @param {FeatureCollection} featureCollection a FeatureCollection for which features will be judged
         * @param {string} type expected GeoJSON type
         * @param {string} name name of calling function
         * @throws {Error} if value is not the expected type.
         */
        function collectionOf(featureCollection, type, name) {
            if (!featureCollection) throw new Error('No featureCollection passed');
            if (!name) throw new Error('.collectionOf() requires a name');
            if (!featureCollection || featureCollection.type !== 'FeatureCollection') {
                throw new Error('Invalid input to ' + name + ', FeatureCollection required');
            }
            for (let i = 0; i < featureCollection.features.length; i++) {
                const feature = featureCollection.features[i];
                if (!feature || feature.type !== 'Feature' || !feature.geometry) {
                    throw new Error('Invalid input to ' + name + ', Feature with geometry required');
                }
                if (!feature.geometry || feature.geometry.type !== type) {
                    throw new Error('Invalid input to ' + name + ': must be a ' + type + ', given ' + feature.geometry.type);
                }
            }
        }

        module.exports.geojsonType = geojsonType;
        module.exports.collectionOf = collectionOf;
        module.exports.featureOf = featureOf;
        module.exports.getCoord = getCoord;
        module.exports.getCoords = getCoords;

    }, {}],
    7: [function (require, module, exports) {
        'use strict'; /* @flow */

        module.exports = cheapRuler;

        /**
         * A collection of very fast approximations to common geodesic measurements. Useful for performance-sensitive code that measures things on a city scale.
         *
         * @param {number} lat latitude
         * @param {string} [units='kilometers']
         * @returns {CheapRuler}
         * @example
         * var ruler = cheapRuler(35.05, 'miles');
         * //=ruler
         */
        function cheapRuler(lat /*: number */, units /*: ?string */) {
            return new CheapRuler(lat, units);
        }

        /**
         * Multipliers for converting between units.
         *
         * @example
         * // convert 50 meters to yards
         * 50 * cheapRuler.units.yards / cheapRuler.units.meters;
         */
        const factors = cheapRuler.units = {
            kilometers: 1,
            miles: 1000 / 1609.344,
            nauticalmiles: 1000 / 1852,
            meters: 1000,
            metres: 1000,
            yards: 1000 / 0.9144,
            feet: 1000 / 0.3048,
            inches: 1000 / 0.0254
        };

        /**
         * Creates a ruler object from tile coordinates (y and z). Convenient in tile-reduce scripts.
         *
         * @param {number} y
         * @param {number} z
         * @param {string} [units='kilometers']
         * @returns {CheapRuler}
         * @example
         * var ruler = cheapRuler.fromTile(1567, 12);
         * //=ruler
         */
        cheapRuler.fromTile = function (y, z, units) {
            const n = Math.PI * (1 - 2 * (y + 0.5) / Math.pow(2, z));
            const lat = Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))) * 180 / Math.PI;
            return new CheapRuler(lat, units);
        };

        function CheapRuler(lat, units) {
            if (lat === undefined) throw new Error('No latitude given.');
            if (units && !factors[units]) throw new Error('Unknown unit ' + units + '. Use one of: ' + Object.keys(factors));

            const m = units ? factors[units] : 1;

            const cos = Math.cos(lat * Math.PI / 180);
            const cos2 = 2 * cos * cos - 1;
            const cos3 = 2 * cos * cos2 - cos;
            const cos4 = 2 * cos * cos3 - cos2;
            const cos5 = 2 * cos * cos4 - cos3;

            // multipliers for converting longitude and latitude degrees into distance (http://1.usa.gov/1Wb1bv7)
            this.kx = m * (111.41513 * cos - 0.09455 * cos3 + 0.00012 * cos5);
            this.ky = m * (111.13209 - 0.56605 * cos2 + 0.0012 * cos4);
        }

        CheapRuler.prototype = {
            /**
             * Given two points of the form [longitude, latitude], returns the distance.
             *
             * @param {Array<number>} a point [longitude, latitude]
             * @param {Array<number>} b point [longitude, latitude]
             * @returns {number} distance
             * @example
             * var distance = ruler.distance([30.5, 50.5], [30.51, 50.49]);
             * //=distance
             */
            distance: function (a, b) {
                const dx = (a[0] - b[0]) * this.kx;
                const dy = (a[1] - b[1]) * this.ky;
                return Math.sqrt(dx * dx + dy * dy);
            },

            /**
             * Returns the bearing between two points in angles.
             *
             * @param {Array<number>} a point [longitude, latitude]
             * @param {Array<number>} b point [longitude, latitude]
             * @returns {number} bearing
             * @example
             * var bearing = ruler.bearing([30.5, 50.5], [30.51, 50.49]);
             * //=bearing
             */
            bearing: function (a, b) {
                const dx = (b[0] - a[0]) * this.kx;
                const dy = (b[1] - a[1]) * this.ky;
                if (!dx && !dy) return 0;
                let bearing = Math.atan2(-dy, dx) * 180 / Math.PI + 90;
                if (bearing > 180) bearing -= 360;
                return bearing;
            },

            /**
             * Returns a new point given distance and bearing from the starting point.
             *
             * @param {Array<number>} p point [longitude, latitude]
             * @param {number} dist distance
             * @param {number} bearing
             * @returns {Array<number>} point [longitude, latitude]
             * @example
             * var point = ruler.destination([30.5, 50.5], 0.1, 90);
             * //=point
             */
            destination: function (p, dist, bearing) {
                const a = (90 - bearing) * Math.PI / 180;
                return this.offset(p,
                    Math.cos(a) * dist,
                    Math.sin(a) * dist);
            },

            /**
             * Returns a new point given easting and northing offsets (in ruler units) from the starting point.
             *
             * @param {Array<number>} p point [longitude, latitude]
             * @param {number} dx easting
             * @param {number} dy northing
             * @returns {Array<number>} point [longitude, latitude]
             * @example
             * var point = ruler.offset([30.5, 50.5], 10, 10);
             * //=point
             */
            offset: function (p, dx, dy) {
                return [
                    p[0] + dx / this.kx,
                    p[1] + dy / this.ky
                ];
            },

            /**
             * Given a line (an array of points), returns the total line distance.
             *
             * @param {Array<Array<number>>} points [longitude, latitude]
             * @returns {number} total line distance
             * @example
             * var length = ruler.lineDistance([
             *     [-67.031, 50.458], [-67.031, 50.534],
             *     [-66.929, 50.534], [-66.929, 50.458]
             * ]);
             * //=length
             */
            lineDistance: function (points) {
                let total = 0;
                for (let i = 0; i < points.length - 1; i++) {
                    total += this.distance(points[i], points[i + 1]);
                }
                return total;
            },

            /**
             * Given a polygon (an array of rings, where each ring is an array of points), returns the area.
             *
             * @param {Array<Array<Array<number>>>} polygon
             * @returns {number} area value in the specified units (square kilometers by default)
             * @example
             * var area = ruler.area([[
             *     [-67.031, 50.458], [-67.031, 50.534], [-66.929, 50.534],
             *     [-66.929, 50.458], [-67.031, 50.458]
             * ]]);
             * //=area
             */
            area: function (polygon) {
                let sum = 0;

                for (let i = 0; i < polygon.length; i++) {
                    const ring = polygon[i];
                    let j = 0;
                    const len = ring.length;
                    let k = len - 1;
                    for (; j < len; k = j++) {
                        sum += (ring[j][0] - ring[k][0]) * (ring[j][1] + ring[k][1]) * (i ? -1 : 1);
                    }
                }

                return (Math.abs(sum) / 2) * this.kx * this.ky;
            },

            /**
             * Returns the point at a specified distance along the line.
             *
             * @param {Array<Array<number>>} line
             * @param {number} dist distance
             * @returns {Array<number>} point [longitude, latitude]
             * @example
             * var point = ruler.along(line, 2.5);
             * //=point
             */
            along: function (line, dist) {
                let sum = 0;

                if (dist <= 0) return line[0];

                for (let i = 0; i < line.length - 1; i++) {
                    const p0 = line[i];
                    const p1 = line[i + 1];
                    const d = this.distance(p0, p1);
                    sum += d;
                    if (sum > dist) return interpolate(p0, p1, (dist - (sum - d)) / d);
                }

                return line[line.length - 1];
            },

            /**
             * Returns an object of the form {point, index} where point is closest point on the line from the given point, and index is the start index of the segment with the closest point.
             *
             * @pointOnLine
             * @param {Array<Array<number>>} line
             * @param {Array<number>} p point [longitude, latitude]
             * @returns {Object} {point, index}
             * @example
             * var point = ruler.pointOnLine(line, [-67.04, 50.5]).point;
             * //=point
             */
            pointOnLine: function (line, p) {
                let minDist = Infinity;
                let minX, minY, minI, minT;

                for (let i = 0; i < line.length - 1; i++) {

                    let x = line[i][0];
                    let y = line[i][1];
                    let dx = (line[i + 1][0] - x) * this.kx;
                    let dy = (line[i + 1][1] - y) * this.ky;

                    if (dx !== 0 || dy !== 0) {

                        var t = ((p[0] - x) * this.kx * dx + (p[1] - y) * this.ky * dy) / (dx * dx + dy * dy);

                        if (t > 1) {
                            x = line[i + 1][0];
                            y = line[i + 1][1];

                        } else if (t > 0) {
                            x += (dx / this.kx) * t;
                            y += (dy / this.ky) * t;
                        }
                    }

                    dx = (p[0] - x) * this.kx;
                    dy = (p[1] - y) * this.ky;

                    const sqDist = dx * dx + dy * dy;
                    if (sqDist < minDist) {
                        minDist = sqDist;
                        minX = x;
                        minY = y;
                        minI = i;
                        minT = t;
                    }
                }

                return {
                    point: [minX, minY],
                    index: minI,
                    t: minT
                };
            },

            /**
             * Returns a part of the given line between the start and the stop points (or their closest points on the line).
             *
             * @param {Array<number>} start point [longitude, latitude]
             * @param {Array<number>} stop point [longitude, latitude]
             * @param {Array<Array<number>>} line
             * @returns {Array<Array<number>>} line part of a line
             * @example
             * var line2 = ruler.lineSlice([-67.04, 50.5], [-67.05, 50.56], line1);
             * //=line2
             */
            lineSlice: function (start, stop, line) {
                let p1 = this.pointOnLine(line, start);
                let p2 = this.pointOnLine(line, stop);

                if (p1.index > p2.index || (p1.index === p2.index && p1.t > p2.t)) {
                    const tmp = p1;
                    p1 = p2;
                    p2 = tmp;
                }

                const slice = [p1.point];

                const l = p1.index + 1;
                const r = p2.index;

                if (!equals(line[l], slice[0]) && l <= r)
                    slice.push(line[l]);

                for (let i = l + 1; i <= r; i++) {
                    slice.push(line[i]);
                }

                if (!equals(line[r], p2.point))
                    slice.push(p2.point);

                return slice;
            },

            /**
             * Returns a part of the given line between the start and the stop points indicated by distance along the line.
             *
             * @param {number} start distance
             * @param {number} stop distance
             * @param {Array<Array<number>>} line
             * @returns {Array<Array<number>>} line part of a line
             * @example
             * var line2 = ruler.lineSliceAlong(10, 20, line1);
             * //=line2
             */
            lineSliceAlong: function (start, stop, line) {
                let sum = 0;
                const slice = [];

                for (let i = 0; i < line.length - 1; i++) {
                    const p0 = line[i];
                    const p1 = line[i + 1];
                    const d = this.distance(p0, p1);

                    sum += d;

                    if (sum > start && slice.length === 0) {
                        slice.push(interpolate(p0, p1, (start - (sum - d)) / d));
                    }

                    if (sum >= stop) {
                        slice.push(interpolate(p0, p1, (stop - (sum - d)) / d));
                        return slice;
                    }

                    if (sum > start) slice.push(p1);
                }

                return slice;
            },

            /**
             * Given a point, returns a bounding box object ([w, s, e, n]) created from the given point buffered by a given distance.
             *
             * @param {Array<number>} p point [longitude, latitude]
             * @param {number} buffer
             * @returns {Array<number>} box object ([w, s, e, n])
             * @example
             * var bbox = ruler.bufferPoint([30.5, 50.5], 0.01);
             * //=bbox
             */
            bufferPoint: function (p, buffer) {
                const v = buffer / this.ky;
                const h = buffer / this.kx;
                return [
                    p[0] - h,
                    p[1] - v,
                    p[0] + h,
                    p[1] + v
                ];
            },

            /**
             * Given a bounding box, returns the box buffered by a given distance.
             *
             * @param {Array<number>} box object ([w, s, e, n])
             * @param {number} buffer
             * @returns {Array<number>} box object ([w, s, e, n])
             * @example
             * var bbox = ruler.bufferBBox([30.5, 50.5, 31, 51], 0.2);
             * //=bbox
             */
            bufferBBox: function (bbox, buffer) {
                const v = buffer / this.ky;
                const h = buffer / this.kx;
                return [
                    bbox[0] - h,
                    bbox[1] - v,
                    bbox[2] + h,
                    bbox[3] + v
                ];
            },

            /**
             * Returns true if the given point is inside in the given bounding box, otherwise false.
             *
             * @param {Array<number>} p point [longitude, latitude]
             * @param {Array<number>} box object ([w, s, e, n])
             * @returns {boolean}
             * @example
             * var inside = ruler.insideBBox([30.5, 50.5], [30, 50, 31, 51]);
             * //=inside
             */
            insideBBox: function (p, bbox) {
                return p[0] >= bbox[0] &&
                    p[0] <= bbox[2] &&
                    p[1] >= bbox[1] &&
                    p[1] <= bbox[3];
            }
        };

        function equals(a, b) {
            return a[0] === b[0] && a[1] === b[1];
        }

        function interpolate(a, b, t) {
            const dx = b[0] - a[0];
            const dy = b[1] - a[1];
            return [
                a[0] + dx * t,
                a[1] + dy * t
            ];
        }

    }, {}],
    8: [function (require, module, exports) {
        // https://d3js.org/d3-collection/ Version 1.0.3. Copyright 2017 Mike Bostock.
        (function (global, factory) {
            typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
                typeof define === 'function' && define.amd ? define(['exports'], factory) :
                    (factory((global.d3 = global.d3 || {})));
        }(this, (function (exports) {
            'use strict';

            const prefix = "$";

            function Map() {
            }

            Map.prototype = map.prototype = {
                constructor: Map,
                has: function (key) {
                    return (prefix + key) in this;
                },
                get: function (key) {
                    return this[prefix + key];
                },
                set: function (key, value) {
                    this[prefix + key] = value;
                    return this;
                },
                remove: function (key) {
                    const property = prefix + key;
                    return property in this && delete this[property];
                },
                clear: function () {
                    for (let property in this) if (property[0] === prefix) delete this[property];
                },
                keys: function () {
                    const keys = [];
                    for (let property in this) if (property[0] === prefix) keys.push(property.slice(1));
                    return keys;
                },
                values: function () {
                    const values = [];
                    for (let property in this) if (property[0] === prefix) values.push(this[property]);
                    return values;
                },
                entries: function () {
                    const entries = [];
                    for (let property in this) if (property[0] === prefix) entries.push({
                        key: property.slice(1),
                        value: this[property]
                    });
                    return entries;
                },
                size: function () {
                    let size = 0;
                    for (let property in this) if (property[0] === prefix)++size;
                    return size;
                },
                empty: function () {
                    for (let property in this) if (property[0] === prefix) return false;
                    return true;
                },
                each: function (f) {
                    for (let property in this) if (property[0] === prefix) f(this[property], property.slice(1), this);
                }
            };

            function map(object, f) {
                const map = new Map;

                // Copy constructor.
                if (object instanceof Map) object.each(function (value, key) {
                    map.set(key, value);
                });

                // Index array by numeric index or specified key function.
                else if (Array.isArray(object)) {
                    let i = -1;
                    const n = object.length;
                    let o;

                    if (f == null) while (++i < n) map.set(i, object[i]);
                    else while (++i < n) map.set(f(o = object[i], i, object), o);
                }

                // Convert object to map.
                else if (object) for (var key in object) map.set(key, object[key]);

                return map;
            }

            let nest = function () {
                const keys = [],
                    sortKeys = [];
                let sortValues,
                    rollup,
                    nest;

                function apply(array, depth, createResult, setResult) {
                    if (depth >= keys.length) return rollup != null
                        ? rollup(array) : (sortValues != null
                            ? array.sort(sortValues)
                            : array);

                    let i = -1;
                    const n = array.length,
                        key = keys[depth++];
                    let keyValue,
                        value;
                    const valuesByKey = map();
                    let values;
                    const result = createResult();

                    while (++i < n) {
                        if (values = valuesByKey.get(keyValue = key(value = array[i]) + "")) {
                            values.push(value);
                        } else {
                            valuesByKey.set(keyValue, [value]);
                        }
                    }

                    valuesByKey.each(function (values, key) {
                        setResult(result, key, apply(values, depth, createResult, setResult));
                    });

                    return result;
                }

                function entries(map$$1, depth) {
                    if (++depth > keys.length) return map$$1;
                    let array;
                    const sortKey = sortKeys[depth - 1];
                    if (rollup != null && depth >= keys.length) array = map$$1.entries();
                    else array = [], map$$1.each(function (v, k) {
                        array.push({ key: k, values: entries(v, depth) });
                    });
                    return sortKey != null ? array.sort(function (a, b) {
                        return sortKey(a.key, b.key);
                    }) : array;
                }

                return nest = {
                    object: function (array) {
                        return apply(array, 0, createObject, setObject);
                    },
                    map: function (array) {
                        return apply(array, 0, createMap, setMap);
                    },
                    entries: function (array) {
                        return entries(apply(array, 0, createMap, setMap), 0);
                    },
                    key: function (d) {
                        keys.push(d);
                        return nest;
                    },
                    sortKeys: function (order) {
                        sortKeys[keys.length - 1] = order;
                        return nest;
                    },
                    sortValues: function (order) {
                        sortValues = order;
                        return nest;
                    },
                    rollup: function (f) {
                        rollup = f;
                        return nest;
                    }
                };
            };

            function createObject() {
                return {};
            }

            function setObject(object, key, value) {
                object[key] = value;
            }

            function createMap() {
                return map();
            }

            function setMap(map$$1, key, value) {
                map$$1.set(key, value);
            }

            function Set() {
            }

            const proto = map.prototype;

            Set.prototype = set.prototype = {
                constructor: Set,
                has: proto.has,
                add: function (value) {
                    value += "";
                    this[prefix + value] = value;
                    return this;
                },
                remove: proto.remove,
                clear: proto.clear,
                values: proto.keys,
                size: proto.size,
                empty: proto.empty,
                each: proto.each
            };

            function set(object, f) {
                const set = new Set;

                // Copy constructor.
                if (object instanceof Set) object.each(function (value) {
                    set.add(value);
                });

                // Otherwise, assume it’s an array.
                else if (object) {
                    let i = -1;
                    const n = object.length;
                    if (f == null) while (++i < n) set.add(object[i]);
                    else while (++i < n) set.add(f(object[i], i, object));
                }

                return set;
            }

            var keys = function (map) {
                const keys = [];
                for (let key in map) keys.push(key);
                return keys;
            };

            var values = function (map) {
                const values = [];
                for (let key in map) values.push(map[key]);
                return values;
            };

            var entries = function (map) {
                const entries = [];
                for (let key in map) entries.push({ key: key, value: map[key] });
                return entries;
            };

            exports.nest = nest;
            exports.set = set;
            exports.map = map;
            exports.keys = keys;
            exports.values = values;
            exports.entries = entries;

            Object.defineProperty(exports, '__esModule', { value: true });

        })));

    }, {}],
    9: [function (require, module, exports) {
        // https://d3js.org/d3-dispatch/ Version 1.0.3. Copyright 2017 Mike Bostock.
        (function (global, factory) {
            typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
                typeof define === 'function' && define.amd ? define(['exports'], factory) :
                    (factory((global.d3 = global.d3 || {})));
        }(this, (function (exports) {
            'use strict';

            const noop = {
                value: function () {
                }
            };

            function dispatch() {
                for (var i = 0, n = arguments.length, _ = {}, t; i < n; ++i) {
                    if (!(t = arguments[i] + "") || (t in _)) throw new Error("illegal type: " + t);
                    _[t] = [];
                }
                return new Dispatch(_);
            }

            function Dispatch(_) {
                this._ = _;
            }

            function parseTypenames(typenames, types) {
                return typenames.trim().split(/^|\s+/).map(function (t) {
                    let name = "";
                    const i = t.indexOf(".");
                    if (i >= 0) name = t.slice(i + 1), t = t.slice(0, i);
                    if (t && !types.hasOwnProperty(t)) throw new Error("unknown type: " + t);
                    return { type: t, name: name };
                });
            }

            Dispatch.prototype = dispatch.prototype = {
                constructor: Dispatch,
                on: function (typename, callback) {
                    const _ = this._,
                        T = parseTypenames(typename + "", _);
                    let t,
                        i = -1;
                    const n = T.length;

                    // If no callback was specified, return the callback of the given type and name.
                    if (arguments.length < 2) {
                        while (++i < n) if ((t = (typename = T[i]).type) && (t = get(_[t], typename.name))) return t;
                        return;
                    }

                    // If a type was specified, set the callback for the given type and name.
                    // Otherwise, if a null callback was specified, remove callbacks of the given name.
                    if (callback != null && typeof callback !== "function") throw new Error("invalid callback: " + callback);
                    while (++i < n) {
                        if (t = (typename = T[i]).type) _[t] = set(_[t], typename.name, callback);
                        else if (callback == null) for (t in _) _[t] = set(_[t], typename.name, null);
                    }

                    return this;
                },
                copy: function () {
                    const copy = {}, _ = this._;
                    for (let t in _) copy[t] = _[t].slice();
                    return new Dispatch(copy);
                },
                call: function (type, that) {
                    if ((n = arguments.length - 2) > 0) for (var args = new Array(n), i = 0, n, t; i < n; ++i) args[i] = arguments[i + 2];
                    if (!this._.hasOwnProperty(type)) throw new Error("unknown type: " + type);
                    for (t = this._[type], i = 0, n = t.length; i < n; ++i) t[i].value.apply(that, args);
                },
                apply: function (type, that, args) {
                    if (!this._.hasOwnProperty(type)) throw new Error("unknown type: " + type);
                    const t = this._[type];
                    let i = 0;
                    const n = t.length;
                    for (; i < n; ++i) t[i].value.apply(that, args);
                }
            };

            function get(type, name) {
                let i = 0;
                const n = type.length;
                let c;
                for (; i < n; ++i) {
                    if ((c = type[i]).name === name) {
                        return c.value;
                    }
                }
            }

            function set(type, name, callback) {
                let i = 0;
                const n = type.length;
                for (; i < n; ++i) {
                    if (type[i].name === name) {
                        type[i] = noop, type = type.slice(0, i).concat(type.slice(i + 1));
                        break;
                    }
                }
                if (callback != null) type.push({ name: name, value: callback });
                return type;
            }

            exports.dispatch = dispatch;

            Object.defineProperty(exports, '__esModule', { value: true });

        })));

    }, {}],
    10: [function (require, module, exports) {
        // https://d3js.org/d3-dsv/ Version 1.0.5. Copyright 2017 Mike Bostock.
        (function (global, factory) {
            typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
                typeof define === 'function' && define.amd ? define(['exports'], factory) :
                    (factory((global.d3 = global.d3 || {})));
        }(this, (function (exports) {
            'use strict';

            function objectConverter(columns) {
                return new Function("d", "return {" + columns.map(function (name, i) {
                    return JSON.stringify(name) + ": d[" + i + "]";
                }).join(",") + "}");
            }

            function customConverter(columns, f) {
                const object = objectConverter(columns);
                return function (row, i) {
                    return f(object(row), i, columns);
                };
            }

            // Compute unique columns in order of discovery.
            function inferColumns(rows) {
                const columnSet = Object.create(null),
                    columns = [];

                rows.forEach(function (row) {
                    for (let column in row) {
                        if (!(column in columnSet)) {
                            columns.push(columnSet[column] = column);
                        }
                    }
                });

                return columns;
            }

            const dsv = function (delimiter) {
                const reFormat = new RegExp("[\"" + delimiter + "\n\r]"),
                    delimiterCode = delimiter.charCodeAt(0);

                function parse(text, f) {
                    let convert, columns;
                    const rows = parseRows(text, function (row, i) {
                        if (convert) return convert(row, i - 1);
                        columns = row, convert = f ? customConverter(row, f) : objectConverter(row);
                    });
                    rows.columns = columns;
                    return rows;
                }

                function parseRows(text, f) {
                    const EOL = {}, // sentinel value for end-of-line
                        EOF = {}, // sentinel value for end-of-file
                        rows = [], // output rows
                        N = text.length;
                    let I = 0, // current character index
                        n = 0, // the current line number
                        t, // the current token
                        eol; // is the current token followed by EOL?

                    function token() {
                        if (I >= N) return EOF; // special case: end of file
                        if (eol) return eol = false, EOL; // special case: end of line

                        // special case: quotes
                        const j = I;
                        let c;
                        if (text.charCodeAt(j) === 34) {
                            let i = j;
                            while (i++ < N) {
                                if (text.charCodeAt(i) === 34) {
                                    if (text.charCodeAt(i + 1) !== 34) break;
                                    ++i;
                                }
                            }
                            I = i + 2;
                            c = text.charCodeAt(i + 1);
                            if (c === 13) {
                                eol = true;
                                if (text.charCodeAt(i + 2) === 10)++I;
                            } else if (c === 10) {
                                eol = true;
                            }
                            return text.slice(j + 1, i).replace(/""/g, "\"");
                        }

                        // common case: find next delimiter or newline
                        while (I < N) {
                            let k = 1;
                            c = text.charCodeAt(I++);
                            if (c === 10) eol = true; // \n
                            else if (c === 13) {
                                eol = true;
                                if (text.charCodeAt(I) === 10)++I, ++k;
                            } // \r|\r\n
                            else if (c !== delimiterCode) continue;
                            return text.slice(j, I - k);
                        }

                        // special case: last token before EOF
                        return text.slice(j);
                    }

                    while ((t = token()) !== EOF) {
                        let a = [];
                        while (t !== EOL && t !== EOF) {
                            a.push(t);
                            t = token();
                        }
                        if (f && (a = f(a, n++)) == null) continue;
                        rows.push(a);
                    }

                    return rows;
                }

                function format(rows, columns) {
                    if (columns == null) columns = inferColumns(rows);
                    return [columns.map(formatValue).join(delimiter)].concat(rows.map(function (row) {
                        return columns.map(function (column) {
                            return formatValue(row[column]);
                        }).join(delimiter);
                    })).join("\n");
                }

                function formatRows(rows) {
                    return rows.map(formatRow).join("\n");
                }

                function formatRow(row) {
                    return row.map(formatValue).join(delimiter);
                }

                function formatValue(text) {
                    return text == null ? ""
                        : reFormat.test(text += "") ? "\"" + text.replace(/\"/g, "\"\"") + "\""
                            : text;
                }

                return {
                    parse: parse,
                    parseRows: parseRows,
                    format: format,
                    formatRows: formatRows
                };
            };

            const csv = dsv(",");

            const csvParse = csv.parse;
            const csvParseRows = csv.parseRows;
            const csvFormat = csv.format;
            const csvFormatRows = csv.formatRows;

            const tsv = dsv("\t");

            const tsvParse = tsv.parse;
            const tsvParseRows = tsv.parseRows;
            const tsvFormat = tsv.format;
            const tsvFormatRows = tsv.formatRows;

            exports.dsvFormat = dsv;
            exports.csvParse = csvParse;
            exports.csvParseRows = csvParseRows;
            exports.csvFormat = csvFormat;
            exports.csvFormatRows = csvFormatRows;
            exports.tsvParse = tsvParse;
            exports.tsvParseRows = tsvParseRows;
            exports.tsvFormat = tsvFormat;
            exports.tsvFormatRows = tsvFormatRows;

            Object.defineProperty(exports, '__esModule', { value: true });

        })));

    }, {}],
    11: [function (require, module, exports) {
        // https://d3js.org/d3-request/ Version 1.0.5. Copyright 2017 Mike Bostock.
        (function (global, factory) {
            typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('d3-collection'), require('d3-dispatch'), require('d3-dsv')) :
                typeof define === 'function' && define.amd ? define(['exports', 'd3-collection', 'd3-dispatch', 'd3-dsv'], factory) :
                    (factory((global.d3 = global.d3 || {}), global.d3, global.d3, global.d3));
        }(this, (function (exports, d3Collection, d3Dispatch, d3Dsv) {
            'use strict';

            let request = function (url, callback) {
                let request;
                const event = d3Dispatch.dispatch("beforesend", "progress", "load", "error");
                let mimeType;
                const headers = d3Collection.map();
                let xhr = new XMLHttpRequest,
                    user = null,
                    password = null,
                    response,
                    responseType,
                    timeout = 0;

                // If IE does not support CORS, use XDomainRequest.
                if (typeof XDomainRequest !== "undefined"
                    && !("withCredentials" in xhr)
                    && /^(http(s)?:)?\/\//.test(url)) xhr = new XDomainRequest;

                "onload" in xhr
                    ? xhr.onload = xhr.onerror = xhr.ontimeout = respond
                    : xhr.onreadystatechange = function (o) {
                        xhr.readyState > 3 && respond(o);
                    };

                function respond(o) {
                    const status = xhr.status;
                    let result;
                    if (!status && hasResponse(xhr)
                        || status >= 200 && status < 300
                        || status === 304) {
                        if (response) {
                            try {
                                result = response.call(request, xhr);
                            } catch (e) {
                                event.call("error", request, e);
                                return;
                            }
                        } else {
                            result = xhr;
                        }
                        event.call("load", request, result);
                    } else {
                        event.call("error", request, o);
                    }
                }

                xhr.onprogress = function (e) {
                    event.call("progress", request, e);
                };

                request = {
                    header: function (name, value) {
                        name = (name + "").toLowerCase();
                        if (arguments.length < 2) return headers.get(name);
                        if (value == null) headers.remove(name);
                        else headers.set(name, value + "");
                        return request;
                    },

                    // If mimeType is non-null and no Accept header is set, a default is used.
                    mimeType: function (value) {
                        if (!arguments.length) return mimeType;
                        mimeType = value == null ? null : value + "";
                        return request;
                    },

                    // Specifies what type the response value should take;
                    // for instance, arraybuffer, blob, document, or text.
                    responseType: function (value) {
                        if (!arguments.length) return responseType;
                        responseType = value;
                        return request;
                    },

                    timeout: function (value) {
                        if (!arguments.length) return timeout;
                        timeout = +value;
                        return request;
                    },

                    user: function (value) {
                        return arguments.length < 1 ? user : (user = value == null ? null : value + "", request);
                    },

                    password: function (value) {
                        return arguments.length < 1 ? password : (password = value == null ? null : value + "", request);
                    },

                    // Specify how to convert the response content to a specific type;
                    // changes the callback value on "load" events.
                    response: function (value) {
                        response = value;
                        return request;
                    },

                    // Alias for send("GET", …).
                    get: function (data, callback) {
                        return request.send("GET", data, callback);
                    },

                    // Alias for send("POST", …).
                    post: function (data, callback) {
                        return request.send("POST", data, callback);
                    },

                    // If callback is non-null, it will be used for error and load events.
                    send: function (method, data, callback) {
                        xhr.open(method, url, true, user, password);
                        if (mimeType != null && !headers.has("accept")) headers.set("accept", mimeType + ",*/*");
                        if (xhr.setRequestHeader) headers.each(function (value, name) {
                            xhr.setRequestHeader(name, value);
                        });
                        if (mimeType != null && xhr.overrideMimeType) xhr.overrideMimeType(mimeType);
                        if (responseType != null) xhr.responseType = responseType;
                        if (timeout > 0) xhr.timeout = timeout;
                        if (callback == null && typeof data === "function") callback = data, data = null;
                        if (callback != null && callback.length === 1) callback = fixCallback(callback);
                        if (callback != null) request.on("error", callback).on("load", function (xhr) {
                            callback(null, xhr);
                        });
                        event.call("beforesend", request, xhr);
                        xhr.send(data == null ? null : data);
                        return request;
                    },

                    abort: function () {
                        xhr.abort();
                        return request;
                    },

                    on: function () {
                        const value = event.on.apply(event, arguments);
                        return value === event ? request : value;
                    }
                };

                if (callback != null) {
                    if (typeof callback !== "function") throw new Error("invalid callback: " + callback);
                    return request.get(callback);
                }

                return request;
            };

            function fixCallback(callback) {
                return function (error, xhr) {
                    callback(error == null ? xhr : null);
                };
            }

            function hasResponse(xhr) {
                const type = xhr.responseType;
                return type && type !== "text"
                    ? xhr.response // null on error
                    : xhr.responseText; // "" on error
            }

            var type = function (defaultMimeType, response) {
                return function (url, callback) {
                    const r = request(url).mimeType(defaultMimeType).response(response);
                    if (callback != null) {
                        if (typeof callback !== "function") throw new Error("invalid callback: " + callback);
                        return r.get(callback);
                    }
                    return r;
                };
            };

            const html = type("text/html", function (xhr) {
                return document.createRange().createContextualFragment(xhr.responseText);
            });

            const json = type("application/json", function (xhr) {
                return JSON.parse(xhr.responseText);
            });

            const text = type("text/plain", function (xhr) {
                return xhr.responseText;
            });

            const xml = type("application/xml", function (xhr) {
                const xml = xhr.responseXML;
                if (!xml) throw new Error("parse error");
                return xml;
            });

            const dsv = function (defaultMimeType, parse) {
                return function (url, row, callback) {
                    if (arguments.length < 3) callback = row, row = null;
                    const r = request(url).mimeType(defaultMimeType);
                    r.row = function (_) {
                        return arguments.length ? r.response(responseOf(parse, row = _)) : row;
                    };
                    r.row(row);
                    return callback ? r.get(callback) : r;
                };
            };

            function responseOf(parse, row) {
                return function (request$$1) {
                    return parse(request$$1.responseText, row);
                };
            }

            const csv = dsv("text/csv", d3Dsv.csvParse);

            const tsv = dsv("text/tab-separated-values", d3Dsv.tsvParse);

            exports.request = request;
            exports.html = html;
            exports.json = json;
            exports.text = text;
            exports.xml = xml;
            exports.csv = csv;
            exports.tsv = tsv;

            Object.defineProperty(exports, '__esModule', { value: true });

        })));

    }, { "d3-collection": 8, "d3-dispatch": 9, "d3-dsv": 10 }]
}, {}, [1]);

flagtablefromatrrquery = false
mapboxgl.accessToken =
    "pk.eyJ1IjoibW9vZGl0YXJzaG91YnkiLCJhIjoiY2p0d3RoeWl1MTV4azQ0cnRhd2owOTE5cyJ9.KTPQNpGdHIdLKHkleT2V6Q";

mapboxgl.setRTLTextPlugin(
    "https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-rtl-text/v0.2.0/mapbox-gl-rtl-text.js"
);

var map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mooditarshouby/cjuz1mb930blj1fqs2wikiybc",
    zoom: 10.5,
    center: [31.29, 29.98778571],
    pitch: 20
});

map.addControl(
    new MapboxGeocoder({
        accessToken: mapboxgl.accessToken,
        mapboxgl: mapboxgl
    })
);
var draw = new MapboxDraw({
    displayControlsDefault: false,
    controls: {
        polygon: true,
        circle: true,
        trash: true
    }
});
map.addControl(draw);
var canvas;
map.addControl(new mapboxgl.NavigationControl());
map.addControl(new mapboxgl.ScaleControl({ position: "bottom-right" }));
//Retrieve configuration for the widgets
var colorcof
var basedoncat = [], basedontype = []
$.getJSON("js/layers.json", function (data) {
    colorcof = data;
    basedontype = ["match",
        ["get", "Type"]]

    var allmentionedtype = colorcof.Type
    for (q = 0; q < allmentionedtype.length; q++) {
        basedontype.push(allmentionedtype[q].name)
        basedontype.push(allmentionedtype[q].color)
    }
    basedontype.push(basecolorforremaining)
    basedoncat = ["match",
        ["get", "Category"]]

    var allmentionedtype = colorcof.Category
    for (q = 0; q < allmentionedtype.length; q++) {
        basedoncat.push(allmentionedtype[q].name)
        basedoncat.push(allmentionedtype[q].color)
    }
    basedoncat.push(basecolorforremaining)
});
var basecolorforremaining = "#ff00ff"
const subcatwithcolor = [];
const filterforCatcolor = ["match", ["get", "Type"]];
var center = [];
let lName;
let oldclicked;
const arr1 = [];
const arr2 = [];
var geoserverlink = 'https://streetdeploy.com:8081/geoserver/cite' //https://streetdeploy.com:8081/geoserver/web/
var layername = 'cite%3Atbl_datapoints'
const mainTypeColour = [];
map.on("load", function (e) {


    canvas = map.getCanvasContainer();
    var datalink
    var allocatedLayersforuser = document.getElementById('allocatedLayers').innerText.split(",")
    if (allocatedLayersforuser[0] != "") {
        if (allocatedLayersforuser[0] == 'all') {
            datalink = geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&outputFormat=application%2Fjson'
        } else {
            Typefilter = 'Type in ('
            for (j = 0; j < allocatedLayersforuser.length; j++) {
                if (j == 0) {
                    Typefilter += "'" + allocatedLayersforuser[j] + "'"
                } else {
                    Typefilter += ",'" + allocatedLayersforuser[j] + "'"
                }
            }
            Typefilter += ")"
            datalink = geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&outputFormat=application%2Fjson&CQL_FILTER=' + Typefilter
        }
    } else {
        document.getElementById('smartsearch').style.display = 'none'
    }

    map.addSource('tbl_data', {
        type: 'geojson',
        data: datalink
    });
    map.addLayer({
        'id': 'pointlayer',
        'type': 'circle',
        'source': 'tbl_data',
        'layout': {},
        'paint': {
            "circle-radius": 2,
            "circle-color": basedontype
        }
    });
    map.addSource('hulls', {
        "type": "geojson",
        "data": {
            "type": "FeatureCollection",
            "features": []
        }
    });
    map
        .addLayer({
            'id': 'quantized',
            'type': 'line',
            'source': 'hulls',
            'layout': {
                'visibility': 'none',
            },
            'paint': {
                'line-color': {
                    "property": "time",
                    "type": 'interval',
                    "stops": colorSchemes.altColor.map(function (pair) {
                        const edited = [pair[0] - 300, pair[1]];
                        return edited
                    })
                },
                'line-opacity': 0.25,
                'line-width': {
                    base: 1.5,
                    "stops": [[10, 1], [22, 4]]
                },
            }
        })
        .addLayer({
            'id': 'quantized-major',
            'type': 'line',
            'source': 'hulls',
            'filter': [">=", 'quantized', state.threshold],
            'layout': {
                'visibility': 'none',
            },
            'paint': {
                'line-color': {
                    "property": "time",
                    "type": 'interval',
                    "stops": colorSchemes.altColor
                },
                'line-width': {
                    base: 1,
                    "stops": [[10, 1.5], [22, 15]]
                }
            },

        })

        .addLayer({
            'id': 'quantized-label',
            'type': 'symbol',
            'source': 'hulls',
            'filter': [">=", 'quantized', state.threshold],
            'layout': {
                'visibility': 'none',
                'text-field': '{minutes} MIN',
                'text-font': ['DIN Offc Pro Bold'],
                'symbol-placement': 'line',
                'text-allow-overlap': true,
                'text-padding': 1,
                'text-max-angle': 90,
                'text-size': {
                    base: 1.2,
                    "stops": [[8, 12], [22, 30]]
                },
                'text-letter-spacing': 0.1
            },
            'paint': {
                'text-halo-color': 'hsl(55, 1%, 20%)',
                'text-color': {
                    "property": "time",
                    "type": 'interval',
                    "stops": colorSchemes.altColor
                },
                'text-halo-width': 12,
            }
        })
        .addLayer({
            'id': 'continuous',
            'type': 'fill',
            'source': 'hulls',
            'paint': {
                'fill-color': ddsColor,
                'fill-opacity': 0
            },
        }, 'water')
        .addLayer({
            'id': 'highlight',
            'type': 'fill',
            'source': nothing,
            'paint': {
                'fill-color': 'rgba(0,0,0,0.5)'
            }
        });
    // const colforlegend = map.getStyle().layers;
    // const blockcolorarray = map.getPaintProperty(
    // "pointlayer",
    // "circle-color"
    // );
    // const divforblock = document.getElementById("forblocklegend");
    // const title = document.createElement("b");
    // title.innerHTML = "Income Levels";
    // divforblock.appendChild(title);
    // const colorgrad = document.createElement("div");
    // colorgrad.style =
    // "background: linear-gradient(to right," +
    // blockcolorarray[4] +
    // " ," +
    // blockcolorarray[blockcolorarray.length - 1] +
    // ");height: 40px;margin:1vh;text-align:left;";
    // colorgrad.innerHTML = 'Low <span style="float:right;">High</span>';
    // divforblock.appendChild(colorgrad);
    // for (i = 0; i < conf["layerid"].length; i++) {
    // subcatwithcolor.push(conf["layerid"][i].name);
    // subcatwithcolor.push(
    // map.getPaintProperty(conf["layerid"][i].name, "circle-color")
    // );
    // }
    // divforblock.style = "display:block";
    // for (i = 0; i < conf["layerinfo"].length; i++) {
    // filterforCatcolor.push(conf["layerinfo"][i].name);
    // filterforCatcolor.push(conf["layerinfo"][i].color);
    // }
    // filterforCatcolor.push("#ffffff");

    const mainCat = [];
    const allLayerIdinfo = [];
    let colorcode;
    features = map.queryRenderedFeatures(e);

    for (i = 0; i < features.length; i++) {
        if (features[i].layer.type == "circle") {
            const type = features[i].properties.Type;
            if (mainCat.includes(type)) {
            } else {
                mainCat.push(type);
                var allLayerId = {};
                allLayerId.name = type;
                allLayerId.id = features[i].layer.id;
                allLayerIdinfo.push(allLayerId);
            }
        } else {
        }
    }

    mainCat.sort();
    console.log(allLayerIdinfo);

    var allLayerId = {};
    allLayerId.name = "pointlayer";
    allLayerId.id = "pointlayer";
    allLayerIdinfo.push(allLayerId);
    var extraLayersFromMapbox
    function addmapboxlayers(){
    $.getJSON("js/mapboxlayers.json", function (data) {
        extraLayersFromMapbox = data.extraLayers;
        const legend = document.getElementById("legends");
        if (extraLayersFromMapbox.length != 0){
            const basemapDiv = document.createElement("div");
            basemapDiv.id = "basemapchecks";
            for(a=0;a<extraLayersFromMapbox.length;a++){
                var onOffdiv = document.createElement("div");
                var x = document.createElement("INPUT");
                x.setAttribute("type", "checkbox");
                x.id = extraLayersFromMapbox[a][1];
                x.checked = true;
                x.className = "Typefrombasemap"
                x.setAttribute("onclick", "showmapboxlayer(this)");
                onOffdiv.appendChild(x);
                var colspan = document.createElement('span')
                colspan.className = 'color'
                colspan.style.backgroundColor = extraLayersFromMapbox[a][2]
                colspan.innerHTML = "&nbsp &nbsp ";
                onOffdiv.appendChild(colspan)
                var p = document.createElement('label')
                p.innerHTML = "&nbsp " + extraLayersFromMapbox[a][0].toUpperCase().trim()
                onOffdiv.appendChild(p)
                basemapDiv.appendChild(onOffdiv)
                
            }
            legend.appendChild(basemapDiv)
        }
    })
}
    $.getJSON(geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&PROPERTYNAME=Category,Type&outputformat=application/json', function (data) {
        console.log(data)
        var allfeat = data.features
        var allTypeArray = []
        for (i = 0; i < allfeat.length; i++) {
            if(allfeat[i].properties["Type"]) {
                var columnval = allfeat[i].properties["Type"].toUpperCase().trim();
                if (allTypeArray.includes(columnval)) {

                } else {
                    allTypeArray.push(columnval)
                }
            }
        }
        var allocatedLayerstoUser = document.getElementById('allocatedLayers').innerText.split(",")
        const legend = document.getElementById("legends");
        var arr3, arr4
        if (allocatedLayerstoUser[0] != "") {
            if (allocatedLayerstoUser[0] == 'all') {
                arr4 = allTypeArray;
            } else {
                arr4 = allocatedLayerstoUser
            }

            const mainDiv = document.createElement("div");
            mainDiv.id = "mainchecks";
            let y = document.createElement("INPUT");
            y.setAttribute("type", "checkbox");
            y.checked = true;
            y.id = 'togglealllayers'
            y.setAttribute("onclick", "Alllayertoggle()");
            var select = document.createElement("label");
            select.innerHTML = "<b > Toggle all layers on/off</b>";
            mainDiv.appendChild(y);
            mainDiv.appendChild(select);
            legend.appendChild(mainDiv);
            arr3 = arr4.sort()
            for (i = 0; i < arr3.length; i++) {
                var onOffdiv = document.createElement("div");
                var x = document.createElement("INPUT");
                x.setAttribute("type", "checkbox");
                x.id = arr3[i];
                x.checked = true;
                x.className = "Typefromgeoserver"
                x.setAttribute("onclick", "sideQuery()");
                onOffdiv.appendChild(x);
                var colspan = document.createElement('span')
                colspan.className = 'color'
                var thecol
                var allmentionedtype = colorcof.Type
                for (z = 0; z < allmentionedtype.length; z++) {
                    if (allmentionedtype[z].name == arr3[i]) {
                        thecol = allmentionedtype[z].color
                    }
                }
                if (thecol == undefined) {
                    thecol = basecolorforremaining
                }
                colspan.style.backgroundColor = thecol
                thecol = undefined
                colspan.innerHTML = "&nbsp &nbsp ";
                onOffdiv.appendChild(colspan)
                var p = document.createElement('label')
                p.innerHTML = "&nbsp " + arr3[i]
                p.setAttribute("onclick", "showHideul(this)");
                onOffdiv.appendChild(p)
                var theUl = document.createElement('ul')
                var alluniqueCategories = []
                for (f = 0; f < allfeat.length; f++) {
                    if(allfeat[f].properties["Type"]) {
                        if (allfeat[f].properties["Type"].toUpperCase().trim() == arr3[i]) {
                            if (alluniqueCategories.includes(allfeat[f].properties["Category"])) {
    
                            } else {
                                (alluniqueCategories.push(allfeat[f].properties["Category"]))
                            }
                        }
                    }
                }
                for (d = 0; d < alluniqueCategories.length; d++) {
                    var li = document.createElement('li')
                    var x = document.createElement("INPUT");
                    x.setAttribute("type", "checkbox");
                    x.setAttribute("checked", "true");
                    x.id = alluniqueCategories[d]
                    x.className = "Categoryfromgeoserver"
                    x.setAttribute("onclick", "sideQuery()");
                    var p = document.createElement('label')
                    p.innerText = alluniqueCategories[d]
                    li.appendChild(x)
                    var colspan = document.createElement('span')
                    colspan.className = 'color'
                    var thecol
                    var allmentionedcategories = colorcof.Category
                    for (w = 0; w < allmentionedcategories.length; w++) {
                        if (allmentionedcategories[w].name == alluniqueCategories[d]) {
                            thecol = allmentionedcategories[w].color
                        }
                    }
                    if (thecol == undefined) {
                        thecol = basecolorforremaining
                    }
                    colspan.style.backgroundColor = thecol
                    thecol = undefined
                    colspan.innerHTML = "&nbsp &nbsp ";
                    li.appendChild(colspan)
                    li.appendChild(p)
                    theUl.appendChild(li)
                    theUl.style.display = 'none'
                }
                onOffdiv.appendChild(theUl)
                legend.appendChild(onOffdiv)
         

            }
            addmapboxlayers()
        } else {

            legend.innerHTML = ' No Layer is assigned for the User. Kindly Contact Administrator'
        }
        map.getCanvas().style.cursor = "default";
    })
    // console.log(mainCat);

    // mainCat.push("malls-0mmu5e");
    // mainCat.push("blocks-classified-region-3ln9lg");


    // // http://192.168.1.9:8080/geoserver/mapbox/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=mapbox%3AMapboxPoints&PROPERTYNAME=Category,Type&outputformat=application/json
    // const legend = document.getElementById("legends");
    // const mainDiv = document.createElement("div");
    // mainDiv.id = "mainchecks";
    // let y = document.createElement("INPUT");
    // y.setAttribute("type", "checkbox");
    // y.checked = true;
    // y.setAttribute("onclick", "Alllayertoggle()");
    // var select = document.createElement("label");
    // select.innerHTML = "<b > Toggle all layers on/off</b>";
    // mainDiv.appendChild(y);
    // mainDiv.appendChild(select);
    // for (i = 0; i < arr3.length; i++) {
    //     let finalid;
    //     const onOffdiv = document.createElement("div");
    //     const x = document.createElement("INPUT");
    //     x.setAttribute("type", "checkbox");
    //     for (k = 0; k < allLayerIdinfo.length; k++) {
    //         if (arr3[i] == allLayerIdinfo[k].name) {
    //             finalid = allLayerIdinfo[k].id;
    //             x.id = finalid;
    //             break;
    //         }
    //     }
    //     x.checked = true;
    //     x.className = "forTypecheck " + arr3[i] + "val";
    //     x.setAttribute(
    //         "onclick",
    //         "showHideType();checkunchecksubcat(this.className)"
    //     );
    //     onOffdiv.appendChild(x);

    //     var select = document.createElement("label");
    //     select.innerHTML = arr3[i];
    //     select.id = arr3[i] + "p";
    //     select.value = [i];
    //     select.setAttribute("onclick", "showHideul(this.id)");
    //     const fromconfig = conf["layerinfo"];
    //     for (y = 0; y < fromconfig.length; y++) {
    //         if (arr3[i] == fromconfig[y].name) {
    //             colorcode = fromconfig[y].color;
    //         }
    //     }
    //     var spanForColor = document.createElement("span");
    //     spanForColor.setAttribute("class", "color");
    //     spanForColor.setAttribute("style", "background-color: " + colorcode);
    //     spanForColor.innerHTML = "&nbsp &nbsp ";
    //     onOffdiv.appendChild(spanForColor);
    //     onOffdiv.appendChild(select);

    //     let gtColorinfo;
    //     const subcatarray = [];
    //     for (j = 0; j < features.length; j++) {
    //         if (features[j].properties.Type == arr3[i]) {
    //             const subcat = features[j].properties.Category;
    //             if (subcatarray.includes(subcat)) {
    //             } else {
    //                 subcatarray.push(subcat);
    //                 gtColorinfo = j;
    //             }
    //         }
    //     }

    //     const ul = document.createElement("ul");
    //     ul.id = [i] + "ul";
    //     ul.style.display = "none";
    //     for (k = 0; k < subcatarray.length; k++) {
    //         const lil = document.createElement("li");
    //         lil.innerHTML +=
    //             " <input type='checkbox' class=" +
    //             finalid +
    //             " onclick='ShowHideLayer()' id='forOnOff'/>";
    //         for (q = 0; q < colforlegend.length; q++) {
    //             if (finalid == colforlegend[q].id) {
    //                 const allpaint = colforlegend[q].paint["circle-color"];
    //                 for (w = 2; w < allpaint.length;) {
    //                     if (subcatarray[k] == allpaint[w][0]) {
    //                         colorcode = allpaint[w + 1];
    //                         break;
    //                     } else {
    //                         colorcode = allpaint[allpaint.length - 1];
    //                         w = w + 2;
    //                     }
    //                 }
    //             }
    //         }
    //         var spanForColor = document.createElement("span");
    //         spanForColor.setAttribute("class", "color");
    //         spanForColor.setAttribute(
    //             "style",
    //             "background-color: " + colorcode
    //         );
    //         spanForColor.innerHTML = "&nbsp &nbsp ";
    //         const spanForName = document.createElement("span");
    //         spanForName.id = "forSpaninLi";
    //         spanForName.innerHTML = subcatarray[k];
    //         lil.appendChild(spanForColor);
    //         lil.appendChild(spanForName);
    //         ul.appendChild(lil);
    //     }

    //     onOffdiv.appendChild(ul);
    //     mainDiv.appendChild(onOffdiv);
    //     legend.appendChild(mainDiv);
    // }
    // document.getElementById("malls-0mmu5e").checked = true;
    //document.getElementById("blocks-classified-region-3ln9lg").checked = true;
    if (sessionStorage.length > 1) {
        map.setZoom(sessionStorage.zoom);
        map.flyTo(sessionStorage.lng, sessionStorage.lat);
    }
});

map.on('draw.create', updateArea);
// map.on('draw.delete', updateArea);
// map.on('draw.update', updateArea);

map.on('draw.modechange', (e) => {
    const data = draw.getAll();
    if (draw.getMode() == 'draw_polygon') {
        var pids = []

        // ID of the added template empty feature
        const lid = data.features[data.features.length - 1].id

        data.features.forEach((f) => {
            if (f.geometry.type === 'Polygon' && f.id !== lid) {
                pids.push(f.id)
            }
        })
        draw.delete(pids)
    }
});

function updateArea(e) {
    if ($.fn.DataTable.isDataTable('#datatbl')) {
        $('#datatbl').DataTable().destroy();
    }

    $('#datatbl tbody').empty();
    var data = draw.getAll();
    var coordinates = data.features[0].geometry.coordinates;
    var searchWithin = turf.polygon([
        coordinates[0]
    ]);
    var pointlayer = map.queryRenderedFeatures({ layers: ['pointlayer'] });
    var ptlayer = []    
    var allptinfo = []
    for (u = 0; u < pointlayer.length; u++) {
        var coord = pointlayer[u].geometry.coordinates
        var prop = pointlayer[u].properties
        var temppoint = turf.point(coord, prop)
        allptinfo.push(temppoint)
    }

    var tablecont = []
    for (g = 0; g < allptinfo.length; g++) {
        if (turf.booleanPointInPolygon(allptinfo[g], searchWithin)) {
            tablecont.push(allptinfo[g])
        }


    }
    var allhead = Object.keys(tablecont[0].properties)
    var allbody = []
    for (f = 0; f < tablecont.length; f++) {
        allbody.push(Object.values(tablecont[f].properties))
    }

    var coumnss = []
    for (v = 0; v < allhead.length; v++) {
        var tempobj = {}
        tempobj.title = allhead[v]
        coumnss.push(tempobj)
    }

    $('#datatbl').DataTable({
        data: allbody,
        columns: coumnss,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
}

function showmapboxlayer(id){
    if (id.checked == false){
        map.setLayoutProperty(id.id, 'visibility', 'none');

    } else {
        map.setLayoutProperty(id.id, 'visibility', 'visible');

    }
   
}
function showHideul(id) {

    const x = id.parentElement.getElementsByTagName('ul')[0]
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

let needToCreateTypelistfromConf = true;
const typeandcolour = ["match", ["get", "Type"]];
let tempinfoOfIdandTypeOfChecked = [];
let needtogetcarcolor = true;

function showHideType() {
    const mainul = document.getElementById("layerz");
    mainul.innerHTML = "";
    needtogetcarcolor = false;

    // console.log(subcatwithcolor);
    let totalboxchecked = 0;
    const div = document.getElementById("mainchecks");
    const allchkbx = div.getElementsByClassName("forTypecheck");
    let infoOfIdandTypeOfChecked = [];
    for (i = 0; i < allchkbx.length; i++) {
        const id = allchkbx[i].id;
        const type = allchkbx[i].parentNode.getElementsByTagName("label")[0]
            .innerText;
        if (allchkbx[i].checked == true) {
            totalboxchecked += 1;
            infoOfIdandTypeOfChecked.push(id);
            infoOfIdandTypeOfChecked.push(type);
        } else {
        }
    }

    if (totalboxchecked > 1) {
        for (i = 0; i < conf["layerid"].length; i++) {
            map.setPaintProperty(
                conf["layerid"][i].name,
                "circle-color",
                filterforCatcolor
            );
        }
        if (needToCreateTypelistfromConf == true) {
            for (j = 0; j < conf["layerinfo"].length; j++) {
                typeandcolour.push(conf["layerinfo"][j].name);
                typeandcolour.push(conf["layerinfo"][j].color);
            }
            typeandcolour.push("#ffffff");
            needToCreateTypelistfromConf = false;
        }
        hidealllayers();
        if (tempinfoOfIdandTypeOfChecked.length > 0) {
            for (j = 0; j < tempinfoOfIdandTypeOfChecked.length;) {
                map.setFilter(tempinfoOfIdandTypeOfChecked[j], undefined);
                j = j + 2;
            }
        }
        if (
            infoOfIdandTypeOfChecked.includes("malls-0mmu5e") &&
            infoOfIdandTypeOfChecked.includes("blocks-classified-region-3ln9lg")
        ) {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "visible");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "visible"
            );
            document.getElementById("forblocklegend").style.display = "block";
        } else if (infoOfIdandTypeOfChecked.includes("malls-0mmu5e")) {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "visible");
            map.setLayoutProperty("malls-0mmu5e copy", "visibility", "visible");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "none"
            );
            document.getElementById("forblocklegend").style.display = "none";
        } else if (
            infoOfIdandTypeOfChecked.includes("blocks-classified-region-3ln9lg")
        ) {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "none");
            map.setLayoutProperty("malls-0mmu5e copy", "visibility", "none");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "visible"
            );
            document.getElementById("forblocklegend").style.display = "block";
        } else {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "none");
            map.setLayoutProperty("malls-0mmu5e copy", "visibility", "none");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "none"
            );
            document.getElementById("forblocklegend").style.display = "none";
        }
        for (j = 0; j < infoOfIdandTypeOfChecked.length;) {
            for (i = 0; i < conf["layerid"].length; i++) {
                let temparrayofcat = [];
                let pushtemparray = ["any"];
                if (infoOfIdandTypeOfChecked[j] == conf["layerid"][i].name) {
                    // map.setPaintProperty(conf["layerid"][i].name, 'circle-color', typeandcolour);
                    for (a = 0; a < infoOfIdandTypeOfChecked.length; a++) {
                        if (
                            infoOfIdandTypeOfChecked[j] ==
                            infoOfIdandTypeOfChecked[a]
                        ) {
                            temparrayofcat.push(
                                infoOfIdandTypeOfChecked[a + 1]
                            );
                        }
                    }
                    if (temparrayofcat.length > 1) {
                        for (z = 0; z < temparrayofcat.length; z++)
                            pushtemparray.push([
                                "==",
                                "Type",
                                temparrayofcat[z]
                            ]);
                        map.setFilter(
                            infoOfIdandTypeOfChecked[j],
                            pushtemparray
                        );
                        pushtemparray = ["any"];
                        temparrayofcat = [];
                    } else {
                        map.setFilter(infoOfIdandTypeOfChecked[j], [
                            "==",
                            "Type",
                            infoOfIdandTypeOfChecked[j + 1]
                        ]);
                    }
                    if (infoOfIdandTypeOfChecked[j] == "malls-0mmu5e") {
                        map.setLayoutProperty(
                            "malls-0mmu5e copy",
                            "visibility",
                            "visible"
                        );
                        map.setLayoutProperty(
                            infoOfIdandTypeOfChecked[j],
                            "visibility",
                            "visible"
                        );
                    } else {
                        map.setLayoutProperty(
                            infoOfIdandTypeOfChecked[j],
                            "visibility",
                            "visible"
                        );
                    }
                }
            }

            j = j + 2;
        }
        tempinfoOfIdandTypeOfChecked = infoOfIdandTypeOfChecked;
        infoOfIdandTypeOfChecked = [];
    } else {
        hidealllayers();
        if (tempinfoOfIdandTypeOfChecked.length > 0) {
            for (j = 0; j < tempinfoOfIdandTypeOfChecked.length;) {
                map.setFilter(tempinfoOfIdandTypeOfChecked[j], undefined);
                j = j + 2;
            }
        }
        for (i = 0; i < subcatwithcolor.length; i++) {
            if (infoOfIdandTypeOfChecked[0] == subcatwithcolor[i]) {
                map.setPaintProperty(
                    infoOfIdandTypeOfChecked[0],
                    "circle-color",
                    subcatwithcolor[i + 1]
                );
                break;
            }
        }
        if (
            infoOfIdandTypeOfChecked.includes("malls-0mmu5e") &&
            infoOfIdandTypeOfChecked.includes("blocks-classified-region-3ln9lg")
        ) {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "visible");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "visible"
            );
            document.getElementById("forblocklegend").style.display = "block";
        } else if (infoOfIdandTypeOfChecked.includes("malls-0mmu5e")) {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "visible");
            map.setLayoutProperty("malls-0mmu5e copy", "visibility", "visible");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "none"
            );
            document.getElementById("forblocklegend").style.display = "none";
        } else if (
            infoOfIdandTypeOfChecked.includes("blocks-classified-region-3ln9lg")
        ) {
            map.setLayoutProperty("malls-0mmu5e", "visibility", "none");
            map.setLayoutProperty("malls-0mmu5e copy", "visibility", "none");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "visible"
            );
            document.getElementById("forblocklegend").style.display = "block";
        } else if (infoOfIdandTypeOfChecked.length != 0) {
            map.setFilter(infoOfIdandTypeOfChecked[0], [
                "==",
                "Type",
                infoOfIdandTypeOfChecked[1]
            ]);
            map.setLayoutProperty(
                infoOfIdandTypeOfChecked[0],
                "visibility",
                "visible"
            );
            document.getElementById("forblocklegend").style.display = "none";
        } else {
            hidealllayers();
            map.setLayoutProperty("malls-0mmu5e", "visibility", "none");
            map.setLayoutProperty("malls-0mmu5e copy", "visibility", "none");
            map.setLayoutProperty(
                "blocks-classified-region-3ln9lg",
                "visibility",
                "none"
            );
            document.getElementById("forblocklegend").style.display = "none";
        }
        tempinfoOfIdandTypeOfChecked = infoOfIdandTypeOfChecked;
    }
    // console.log(typeandcolour)
}

map.on("click", function (e) {
    clearMap();
    clng = e.lngLat.lng;
    clat = e.lngLat.lat;
    c = [clng, clat];
    marker.setLngLat(c).addTo(map);
    // map.easeTo({ center: c, duration: 2000 });

    const bbox = [
        [e.point.x - 10, e.point.y - 10],
        [e.point.x + 10, e.point.y + 10]
    ];
    const ff = map.queryRenderedFeatures(bbox);
    if (ff.length > 0) {
        if ((ff[0].layer.type = "circle")) {
            // console.log(ff);
            const coordinates = ff[0].geometry.coordinates;
            if (typeof (coordinates[0]) == "number") {
                const str =
                    "<b>Name :</b>" +
                    ff[0].properties.Name +
                    "<br><b>Category :</b>" +
                    ff[0].properties.Type +
                    "<br><b> Subcategory :</b>" +
                    ff[0].properties.Category +
                    "<br><b>Phone :</b>" +
                    ff[0].properties.Phone;
                if (str != undefined) {
                    Popup();

                    function Popup() {
                        new mapboxgl.Popup({
                            offset: [0, -30]
                        })
                            .setLngLat(coordinates)
                            .setHTML(str)
                            .addTo(map);

                    }
                }
            }
        }
    }
    // clearMap();
    // isochrone(c, state, visualize);
    // d3.select('#loader')
    //   .classed('hidden', false)


});

function checkunchecksubcat(value) {
    const allcheckboxes = document.getElementsByTagName("input");
    for (i = 0; i < allcheckboxes.length; i++) {
        if (allcheckboxes[i].id == "forOnOff") {
            allcheckboxes[i].checked = false;
        }
    }
    const val = value;
    const valarr = val.split(" ");
    //alert(valarr)
    const allinp = document
        .getElementsByClassName(valarr[1])[0]
        .parentNode.getElementsByTagName("input");
    if (document.getElementsByClassName(value)[0].checked == true) {
        for (i = 1; i < allinp.length; i++) {
            document
                .getElementsByClassName(valarr[1])[0]
                .parentNode.getElementsByTagName("input")[i].checked = true;
        }
    } else {
        for (i = 1; i < allinp.length; i++) {
            document
                .getElementsByClassName(valarr[1])[0]
                .parentNode.getElementsByTagName("input")[i].checked = false;
        }
    }
}

let fordetails = [];
const needtogetcolorarray = true;

function ShowHideLayer() {
    const allsubcat = document.getElementsByTagName("input");
    hidealllayers();
    fordetails = [];
    let temparrayofcat = [];
    let pushtemparray = ["any"];
    const idandnameofsubcat = [];
    let tempidandnameofsubcat = [];
    for (i = 0; i < allsubcat.length; i++) {
        if (allsubcat[i].checked == true) {
            if (allsubcat[i].id == "forOnOff") {
                const idofsubcat = allsubcat[i].className;
                const nameofsubcat = allsubcat[i].parentNode.getElementsByTagName(
                    "span"
                )[1].innerText;
                idandnameofsubcat.push(idofsubcat);
                idandnameofsubcat.push(nameofsubcat);
                fordetails.push(nameofsubcat);
            }
        }
    }

    for (j = 0; j < idandnameofsubcat.length;) {
        for (a = 0; a < idandnameofsubcat.length; a++) {
            if (idandnameofsubcat[j] == idandnameofsubcat[a]) {
                temparrayofcat.push(idandnameofsubcat[a + 1]);
            }
        }
        for (z = 0; z < temparrayofcat.length; z++) {
            pushtemparray.push(["==", "Category", temparrayofcat[z]]);
        }
        for (e = 0; e < subcatwithcolor.length; e++) {
            if (idandnameofsubcat[j] == subcatwithcolor[e]) {
                map.setPaintProperty(
                    idandnameofsubcat[j],
                    "circle-color",
                    subcatwithcolor[e + 1]
                );
            }
        }
        map.setFilter(idandnameofsubcat[j], pushtemparray);
        pushtemparray = ["any"];
        temparrayofcat = [];

        tempidandnameofsubcat = idandnameofsubcat;

        map.setLayoutProperty(idandnameofsubcat[j], "visibility", "visible");
        j = j + 2;
    }
}

// function ShowHideLayer(id) {
//     var allsubcat = document.getElementsByClassName(id)
//     var filterarray = ['any']
//     fordetails = []
//     for (i = 0; i < allsubcat.length; i++) {

//         if (allsubcat[i].checked == true) {
//             var nameofsubcat = allsubcat[i].parentNode.getElementsByTagName('span')[0].innerText
//             filterarray.push(['==', 'Category', nameofsubcat])
//             fordetails.push(nameofsubcat)

//         }
//     }
//     hidealllayers()
//     map.setFilter(id, filterarray)
//     map.setLayoutProperty(id, 'visibility', 'visible');
//     filterarray = ['any']

// }

function hidealllayers() {
    for (i = 0; i < conf["layerid"].length; i++) {
        map.setLayoutProperty(conf["layerid"][i].name, "visibility", "none");
    }
}

function showalllayers() {
    for (i = 0; i < conf["layerid"].length; i++) {
        map.setLayoutProperty(conf["layerid"][i].name, "visibility", "visible");
    }
}

// map.on('render', function(){

// })

function getdetails() {
    var allselectedcategorie = $('.Categoryfromgeoserver:checkbox:checked')
    var cqlfilter = '1 = 1 '

    var Catfilter = ''

    if (allselectedcategorie.length != 0) {
        Catfilter = 'Category in ('
        for (j = 0; j < allselectedcategorie.length; j++) {
            if (j == 0) {
                Catfilter += "'" + allselectedcategorie[j].id + "'"
            } else {
                Catfilter += ",'" + allselectedcategorie[j].id + "'"
            }
        }
        Catfilter += ")"
    }

    if (Catfilter != '') {
        cqlfilter = Catfilter
    } else {
        cqlfilter = "Type = 'dxv'"
    }

    // get all features from geoserver 
    $.getJSON(geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&PROPERTYNAME=Category,Type&outputFormat=application%2Fjson&CQL_FILTER=bbox(geom,' + map.getBounds()._sw["lng"] + ',' + map.getBounds()._sw["lat"] + ',' + map.getBounds()._ne["lng"] + ',' + map.getBounds()._ne["lat"] + ') and ' + cqlfilter, function (data) {
        console.log(data)

        if (data.length == 0) {
            alert('No category is selected')
        } else {
            var allFeatures = data.features
            var allselectedcategorie = $('.Categoryfromgeoserver:checkbox:checked')
            const mainul = document.getElementById("layerz");
            mainul.innerHTML = "";
            let colornum;
            var colorcode;
            for (i = 0; i < allselectedcategorie.length; i++) {
                var maintype
                var li = document.createElement("li");
                var spanForDetails = document.createElement("span");
                spanForDetails.innerText = allselectedcategorie[i].id
                spanForDetails.setAttribute(
                    "style",
                    "font-family:'Calibri (Body)'"
                );

                li.appendChild(spanForDetails)
                var totalitems = 0
                for (f = 0; f < allFeatures.length; f++) {
                    if (allFeatures[f].properties.Category == allselectedcategorie[i].id) {
                        totalitems++
                        maintype = allFeatures[f].properties.Type.toUpperCase().trim()
                    }
                }
                if(totalitems == 0) {
                    maintype = allselectedcategorie[i].parentElement.parentElement.parentElement.children[0].id
                }
                const spanForTotalItemInDisplay = document.createElement("span");
                spanForTotalItemInDisplay.setAttribute("class", "num");
                spanForTotalItemInDisplay.innerText = totalitems
                totalitems = 0
                li.appendChild(spanForTotalItemInDisplay)
                var SpanOfType = document.createElement("span");
                SpanOfType.innerText = maintype
                SpanOfType.setAttribute("Style", "font-size:8px;color:grey");
                li.appendChild(SpanOfType)
                mainul.appendChild(li)
            }
        }

    })
}

function home() {
    map.setZoom(10.5);
    map.setCenter([31.29, 29.98778571]);
}

let flagturnonall = false;
var types = ["Certified Public Accountant",
    "Accounting firm",
    "Engineering consultant",
    "Amusement center",
    "Accountant",
    "Art Gallery",
    "ATM",
    "Taxi service",
    "Chartered Accountant",
    "Financial Institution",
    "Point of interest",
    "Financial Audit",
    "Research institute",
    "Corporate office",
    "Financial consultant",
    "Auditor",
    "Amusement park",
    "Water park",
    "Bookkeeping service",
    "Billiards supply store",
    "Theme park",
    "City park",
    "Business management consultant",
    "Park",
    "Garden",
    "Accounting software company",
    "Shop supermarket furniture store",
    "Bank",
    "Picture frame shop",
    "Human Resource Consulting",
    "Children's Amusement Center",
    "Donut shop",
    "Bakery",
    "Art center",
    "Patisserie",
    "Pastry shop",
    "Money transfer service",
    "Art supply store",
    "Investment Bank",
    "Museum",
    "Savings Bank",
    "Cupcake shop",
    "Cake shop",
    "Beauty Salon",
    "Fashion accessories store",
    "Print shop",
    "Restaurant",
    "Sports complex",
    "Transcription service",
    "Transportation service",
    "Aviation consultant",
    "Dessert shop",
    "Airline",
    "Cafe",
    "Coffee shop",
    "Sweets and dessert buffet",
    "French restaurant",
    "Cookie shop",
    "Training centre",
    "Car dealer",
    "BMW dealer",
    "Ice cream shop",
    "Pancake restaurant",
    "Store",
    "Land Rover dealer",
    "Food and Drink",
    "Car rental agency",
    "Car Repair and Maintenance",
    "Auto repair shop",
    "Tire shop",
    "Car battery store",
    "Tuning Automobile",
    "Brake shop",
    "Motorcycle repair shop",
    "Wheel store",
    "Candy store",
    "Car Wash",
    "Car detailing service",
    "Clothing store",
    "Women's Clothing Store",
    "Baby store",
    "Work clothes store",
    "Baby clothing store",
    "Bankruptcy service",
    "Perfume store",
    "Pharmacy",
    "Townhouse complex",
    "E-commerce Service",
    "Bridal shop",
    "Sporting goods store",
    "Bar",
    "Venture capital company",
    "Currency exchange service",
    "Bar & Grill",
    "Convenience store",
    "Dental clinic",
    "Japanese restaurant",
    "Dentist",
    "Cosmetic Dentist",
    "Lounge",
    "Carpet store",
    "Karaoke Bar",
    "Department store",
    "Physician",
    "Contractor",
    "Oral surgeon",
    "Entertainment",
    "Plastic fabrication company",
    "Website designer",
    "Ophthalmologist",
    "Dermatologist",
    "Otolaryngologist",
    "Medical clinic",
    "College",
    "Electrician",
    "Electrical supply store",
    "Software company",
    "Electronics store",
    "Coworking Space",
    "Research Foundation",
    "Home theater store",
    "Student Dormitory",
    "Portrait studio",
    "Marketing agency",
    "Nail Salon",
    "Real Estate Developer",
    "Computer store",
    "Cell phone accessory store",
    "Educational consultant",
    "Middle Eastern restaurant",
    "Pub",
    "Hair Salon",
    "Translator",
    "Translation company",
    "School",
    "Fire station",
    "Cocktail Bar",
    "Bicycle Shop",
    "Kitchen furniture store",
    "Mattress store",
    "Book store",
    "Furniture store",
    "Stationery store",
    "Kitchen supply store",
    "Office furniture store",
    "Disco club",
    "Gas station",
    "Italian restaurant",
    "Health",
    "Alternative fuel station",
    "Childrens book store",
    "Gym",
    "Gymnastics center",
    "Gymnastics club",
    "Martial arts school",
    "Comic book store",
    "Health club",
    "Spa",
    "Hairdresser",
    "Hair Care",
    "Skin care clinic",
    "Barber shop",
    "Laser hair removal service",
    "Home goods store",
    "Outdoor furniture store",
    "Wholesaler",
    "Bathroom supply store",
    "Bedding store",
    "Importer",
    "Wellness center",
    "Hospital",
    "Hair removal service",
    "Cosmetics store",
    "Make-up Artist",
    "Medical Center",
    "Private hospital",
    "Emergency room",
    "Children's Hospital",
    "Landscape Architect",
    "Fast food restaurant",
    "Insurance company",
    "Bicycle repair shop",
    "Employment agency",
    "Motorcycle shop",
    "Used motorcycle dealer",
    "Gift shop",
    "Motor scooter dealer",
    "Shopping Mall",
    "Motorcycle parts store",
    "Campground",
    "Holiday park",
    "Camp",
    "Nursery school",
    "Educational Institution",
    "Post office",
    "Video Arcade",
    "Youth organization",
    "Jewelry Exporter",
    "Dog park",
    "Hotel",
    "Laundromat",
    "Dry Cleaner",
    "Laundry",
    "Law firm",
    "Bus station",
    "Library",
    "Beer store",
    "Local Government Offices",
    "Department of Motor Vehicles",
    "Passport office",
    "Used car dealer",
    "District office",
    "City employment department",
    "Architect",
    "Public works department",
    "Company Registry",
    "lodging ",
    "Rare book store",
    "Indoor Lodging",
    "Serviced Accommodation",
    "Ford dealer",
    "Lodging",
    "Toyota dealer",
    "Motor vehicle dealer",
    "Honda dealer",
    "Mercedes Benz dealer",
    "Audi dealer",
    "Christian book store",
    "Religious book store",
    "Market",
    "Hyundai dealer",
    "Mazda dealer",
    "Auto broker",
    "Auto market",
    "Chevrolet dealer",
    "Car Rental",
    "Fried chicken takeaway",
    "Pizza restaurant",
    "Seafood restaurant",
    "Mosque",
    "BPO company",
    "Medical Group",
    "Camera store",
    "Office equipment rental service",
    "Swimming Pool",
    "Movie theater",
    "Accounting",
    "Mechanic",
    "Auto body shop",
    "Car service",
    "Cafeteria",
    "Oil change service",
    "Freight forwarding service",
    "Auto parts store",
    "Import export company",
    "Shipping company",
    "Auto glass shop",
    "Courier service",
    "Window installation service",
    "Shipping service",
    "Coffee store",
    "Auto air conditioning service",
    "Bus company",
    "Lebanese restaurant",
    "Mover",
    "Pump supplier",
    "Auto restoration service",
    "Family restaurant",
    "Marketing consultant",
    "Car inspection station",
    "Archaeological site",
    "Racing car parts store",
    "Truck repair shop",
    "Radiator repair service",
    "Air conditioning repair service",
    "Night club",
    "Scooter repair shop",
    "Auto tune up service",
    "Auto machine shop",
    "Car accessories store",
    "Paint store",
    "Art school",
    "Laboratory",
    "Auto body parts supplier",
    "Office",
    "Auto electrical service",
    "Battery store",
    "Hookah Bar",
    "Parking Lot",
    "Internet cafe",
    "Airport Parking Lot",
    "Parking Garage",
    "Physical therapy clinic",
    "Auto Upholsterer",
    "Plumbing supply store",
    "Real estate consultant",
    "Real estate agency",
    "Industrial real estate agency",
    "Commercial real estate agency",
    "English Language Camp",
    "Wildlife and safari park",
    "Real Estate Agents",
    "Business to business service",
    "Casino",
    "Orthodox Church",
    "Evangelical Church",
    "Catholic Church",
    "Church",
    "Sushi restaurant",
    "Christian Church",
    "City government office",
    "Public safety office",
    "Government office",
    "District government office",
    "Police department",
    "Egyptian restaurant",
    "Council",
    "Federal government office",
    "Public medical center",
    "Municipal Department of Sports",
    "Syrian restaurant",
    "Legal Affairs Bureau",
    "Water utility company",
    "Barbecue restaurant",
    "Asian restaurant",
    "Fish & Chips Restaurant",
    "Employment center",
    "Hamburger restaurant",
    "County government office",
    "Customs department",
    "Marriage License Bureau",
    "City Department of Transportation",
    "Jeep dealer",
    "Regional Council",
    "Labor Union",
    "University",
    "Men's Clothing Store",
    "Car finance and loan company",
    "Sports club",
    "Interior designer",
    "Notary public",
    "Shoe store",
    "Environment office",
    "Outlet Mall",
    "Office equipment supplier",
    "Children's Clothing Store",
    "Pie shop",
    "Sportswear store",
    "Butcher shop",
    "Company",
    "Video game store",
    "Uniform store",
    "Appliance store",
    "Building materials store",
    "Spice store",
    "Grocery store",
    "Costume store",
    "Dental supply store",
    "Car stereo store",
    "Herb shop",
    "Medical supply store",
    "Beauty supply store",
    "Vitamin & Supplements Store",
    "Transportation Infrastructure",
    "Vaporizer store",
    "Tobacco shop",
    "Variety store",
    "Chocolate shop",
    "Gourmet grocery store",
    "Tour Operator",
    "Limousine service",
    "Toy store",
    "Travel agency",
    "Watch store",
    "Supermarket",
    "Greengrocer",
    "Organic drug store",
    "Auto parts market",
    "Courthouse",
    "City Courthouse",
    "Security system supplier",
    "Animal feed store",
    "Smart shop",
    "Sports nutrition store",
    "Diesel engine repair service",
    "Skateboard shop",
    "Tattoo shop",
    "Used auto parts store",
    "Organic food store",
    "Toy and game manufacturer",
    "Car alarm supplier",
    "Transmission shop",
    "Auto parts manufacturer",
    "Radiator shop",
    "Hardware store",
    "Fishing Charter",
    "Sandwich shop",
    "Self Service Car Wash",
    "Horseback riding service",
    "Grocery delivery service",
    "Presbyterian Church",
    "Medical equipment supplier",
    "Dental Implants Periodontist",
    "Telecommunications equipment supplier",
    "Anglican Church",
    "Transit station",
    "Assemblies of God Church",
    "Subway station",
    "Minibus taxi service",
    "Dental school",
    "Mailing service",
    "Full Gospel Church",
    "Greek Orthodox Church",
    "Eastern Orthodox Church",
    "Tour agency",
    "Friends Church",
    "Seventh-day Adventist Church",
    "Church of Christ",
    "Youth clothing store",
    "Travel Services",
    "Airline ticket agency",
    "Protestant Church",
    "Baptist Church",
    "Armenian Church",
    "Reformed Church",
    "Church of Jesus Christ of Latter-day Saints",
    "Monastery",
    "Lingerie store",
    "Custom T-shirt Store",
    "Underwear store",
    "Boutique",
    "Dress store",
    "Childrens store",
    "Tuxedo shop",
    "Plastic surgeon",
    "Pediatrician",
    "Nutritionist",
    "Gastroenterologist",
    "Exercise equipment store",
    "Obstetrician-Gynecologist",
    "Surgeon",
    "Clothing wholesale market place",
    "Pulmonologist",
    "Neonatal physician",
    "Physiatrist",
    "Neurologist",
    "Sports medicine physician",
    "Cardiologist",
    "Mental health clinic",
    "Psychiatrist",
    "Fertility clinic",
    "Neurosurgeon",
    "Radiologist",
    "Lasik surgeon",
    "Specialized clinic",
    "Endocrinologist",
    "Home health care service",
    "Eye care center",
    "Surgical center",
    "Family Court",
    "Magistrates' Court",
    "Electrical installation service",
    "Electrical equipment supplier",
    "Cleaning service",
    "Fire protection system supplier",
    "Dental Laboratory",
    "Periodontist",
    "Orthodontist",
    "Recording studio",
    "Music store",
    "Musical instrument store",
    "Photo shop",
    "Computer software store",
    "Computer service",
    "Audio visual equipment supplier",
    "Computer networking center",
    "Musician",
    "Computer Support and Services",
    "Electronics repair shop",
    "Foreign Consulate",
    "Embassy",
    "Cemetery",
    "Rheumatologist",
    "Pain management physician",
    "Florist",
    "Vascular surgeon",
    "Internist",
    "Ophthalmology clinic",
    "Women's Health Clinic",
    "Orthopedic clinic",
    "Cancer treatment center",
    "Electrical engineer",
    "Appliance repair service",
    "HVAC Contractor",
    "Furniture manufacturer",
    "Volkswagen dealer",
    "Paintings store",
    "Photo lab",
    "Commercial Printer",
    "Electronic parts supplier",
    "Computer repair service",
    "Gas shop",
    "Electronics vending machine",
    "Cell phone store",
    "Computer consultant",
    "Video store",
    "Security service",
    "Martial arts club",
    "Wedding Planner",
    "Flower designer",
    "Event Planner",
    "Dome",
    "Antique furniture store",
    "Bedroom furniture store",
    "Bar restaurant furniture store",
    "Furniture accessories supplier",
    "Furniture Accessories",
    "Duty free store",
    "Lighting store",
    "Amish furniture store",
    "Hotel supply store",
    "Linens store",
    "Home improvement store",
    "Furniture maker",
    "Bed shop",
    "Fuel supplier",
    "Specialized hospital",
    "General hospital",
    "Manufacturer",
    "Insurance agency",
    "Physical fitness program",
    "Yoga studio",
    "Boxing Gym",
    "Association or organization",
    "District court",
    "Insurance broker",
    "Judo school",
    "Jewelry store",
    "Jeweler",
    "Community center",
    "Dance school",
    "Jewelry designer",
    "Fitness equipment wholesaler",
    "Laundry service",
    "Plastic surgery clinic",
    "Upholstery cleaning service",
    "Wig shop",
    "Hair replacement service",
    "Tool store",
    "Machine shop",
    "Marine supply store",
    "Dyeworks",
    "Metal Services",
    "Iron Works",
    "Janitorial service",
    "Synagogue",
    "Attorney",
    "Antique store",
    "General Practice Attorney",
    "University hospital",
    "Immigration Attorney",
    "Legal Services",
    "Criminal Justice Attorney",
    "Psychiatric hospital",
    "District Attorney",
    "Trial Attorney",
    "Public library",
    "University library",
    "Liquor store",
    "Wine store",
    "State government office",
    "Government hospital",
    "Municipal Department Agricultural Development",
    "Municipal health department",
    "Visa and passport office",
    "Public health department",
    "Public Defender's Office",
    "State Department of Tourism",
    "Tax Collector's Office",
    "Housing Authority",
    "Department of Public Safety",
    "License Bureau",
    "Regional government office",
    "Department of Social Services",
    "Immigration & Naturalization Service",
    "Tax department",
    "Social security office",
    "Municipal Department Agriculture Food Supply",
    "State Dept of Sports",
    "Health insurance agency",
    "Veterans affairs department",
    "Department of Transportation",
    "Urban planning department",
    "Medical certificate service",
    "State office of education",
    "Driver's License Office",
    "Public University",
    "Agenzia Entrate",
    "City Hall",
    "Safety equipment supplier",
    "City tax office",
    "Home audio store",
    "Mobile phone repair shop",
    "Municipal Department of Tourism",
    "Television repair service",
    "Language school",
    "Locksmith",
    "Key duplication service",
    "Live Music Venue",
    "Costume jewelry shop",
    "Wholesale Jeweler",
    "Silversmith",
    "Pizza Delivery",
    "Izakaya restaurant",
    "Civil Police",
    "Delivery Restaurant",
    "Caterer",
    "Meal takeaway",
    "House cleaning service",
    "Carpet cleaning service",
    "Pizza takeaway",
    "Dried flower shop",
    "Candle store",
    "Barrister",
    "Administrative Attorney",
    "Divorce Lawyer",
    "Labor Relations Attorney",
    "Family Law Attorney",
    "Employment Attorney",
    "Conference center",
    "Indian restaurant",
    "Restaurant supply store",
    "Law library",
    "Childrens library",
    "Non-Profit Organization",
    "National library",
    "Chinese restaurant",
    "Used furniture store",
    "Copy shop",
    "Fish and chips takeaway",
    "State liquor store",
    "Chinese takeaway",
    "Latin American restaurant",
    "Chicken restaurant",
    "Registration office",
    "Hot pot restaurant",
    "Railroad company",
    "Municipal office education",
    "State Department of Transportation",
    "Office of Vital Records",
    "Condominium complex",
    "Intellectual Property Registry",
    "Chamber of Commerce",
    "Moving and storage service",
    "Trucking company",
    "Engineer",
    "Construction company",
    "Logistics",
    "Self defense school",
    "Delivery service",
    "Sports",
    "Logistics service",
    "War museum",
    "Historical place museum",
    "Natural history museum",
    "National museum",
    "Technology museum",
    "Children's Museum",
    "Art museum",
    "Handicraft museum",
    "Archaeological museum",
    "Army museum",
    "Club",
    "Painter",
    "Painting",
    "Beauty school",
    "Health and beauty shop",
    "American restaurant",
    "Parking",
    "Pet supply store",
    "Pet store",
    "Yemenite restaurant",
    "Bird shop",
    "Butcher shop deli",
    "Crêperie",
    "Hot dog stand",
    "Homeopathic Pharmacy",
    "Maternity hospital",
    "Snack Bar",
    "Religious Destination",
    "Pharmaceutical products wholesaler",
    "Hearing aid repair service",
    "Pharmaceutical company",
    "Plumber",
    "Movie rental store",
    "Rail museum",
    "Museum of Zoology",
    "Real Estate",
    "Tourist Attraction",
    "History museum",
    "Heritage museum",
    "Office space rental agency",
    "Real estate rental agency",
    "Apartment rental agency",
    "Property Investment",
    "Property management company",
    "Condominium rental agency",
    "Tea house",
    "Cabaret club",
    "Southeast Asian restaurant",
    "Powder coating service",
    "Advertising agency",
    "Botanical Garden",
    "State park",
    "Community Garden",
    "Memorial park",
    "Zoo",
    "Fountain",
    "National Forest",
    "Arboretum",
    "Steak house",
    "International school",
    "Private school",
    "School center",
    "Parking Lot for Bicycles",
    "Flight school",
    "Elementary school",
    "Charter school",
    "Middle school",
    "Public Parking Space",
    "Engineering school",
    "Boot store",
    "Physical Therapist",
    "University department",
    "Felt boots store",
    "Leather goods store",
    "Real estate school",
    "Mobile home rental agency",
    "Housing Development",
    "Internet marketing service",
    "Interior architect office",
    "Spa and health club",
    "Shelving store",
    "Dairy store",
    "Game store",
    "Buffet restaurant",
    "Brasserie",
    "Fishing store",
    "Gun shop",
    "Pen store",
    "Swimwear store",
    "Sunglasses store",
    "Thai restaurant",
    "Mediterranean restaurant",
    "MINI dealer",
    "Mexican restaurant",
    "Swiss restaurant",
    "Luggage store",
    "Flooring Contractor",
    "High school",
    "Boys' High School",
    "Music school",
    "Meat products",
    "Drama school",
    "Countertop store",
    "Computer training school",
    "Mobile home supply store",
    "Metropolitan train company",
    "Special education school",
    "Hospitality and tourism school",
    "Suburban Train Line",
    "Kindergarten",
    "Education center",
    "Learning center",
    "Hypermarket",
    "Medical school",
    "Media and Information Sciences Faculty",
    "Catholic school",
    "Discount Supermarket",
    "Preparatory school",
    "Aged Care",
    "Primary school",
    "Hall",
    "Performing arts theater",
    "Train station",
    "Transit Stop",
    "SCUBA tour agency",
    "Orthopedic shoe store",
    "Bag shop",
    "Retail space rental agency",
    "Whale watching tour agency",
    "Small claims assistance service",
    "Veterinarian",
    "Veterinary Pharmacy",
    "Book Publisher",
    "Animal hospital",
    "Day spa",
    "Massage spa",
    "Veterinary Care",
    "Emergency veterinarian service",
    "Medical spa",
    "Sports Massage Therapist",
    "Martial arts supply store",
    "Electronics company",
    "Veterans organization",
    "Pet Groomer",
    "Balloon store",
    "Magazine store",
    "Office supply store",
    "Upholstery shop",
    "Tax consultant",
    "Health food store",
    "Nut store",
    "Optician",
    "Outdoor sports store",
    "Body piercing shop",
    "Surplus store",
    "Shipping and mailing service",
    "Outlet store",
    "Ferry terminal",
    "Confectionery",
    "Wedding Venue",
    "Bus tour agency",
    "Sightseeing tour agency",
    "Consumer advice center",
    "Banking and Finance",
    "Visa consultant",
    "Bus ticket agency",
    "Hostel",
    "International trade consultant",
    "Gold dealer",
    "Small plates restaurant",
    "Debt collection agency",
    "Breakfast restaurant",
    "Bank or ATM",
    "Traditional restaurant",
    "Espresso Bar",
    "Telecommunications Service Provider",
    "Telecommunications Contractor",
    "Eatery",
    "Kosher restaurant",
    "Business networking company",
    "Telecommunications",
    "Telephone Exchange",
    "Internet Service Provider",
    "Telephone company",
    "Mobile Network Operator",
    "Motorcycle dealer",
    "Bowling Alley",
    "Dessert restaurant",
    "Grand cafe",
    "Nissan dealer",
    "Jaguar dealer",
    "Subaru dealer",
    "ATV dealer",
    "Smart dealer",
    "Diesel engine dealer",
    "Trust Bank",
    "Maternity store",
    "Fashion designer",
    "Clothes market",
    "Wedding store",
    "Natural goods store",
    "Produce market",
    "Dental Hygienist",
    "X-ray Lab",
    "Prosthodontist",
    "Plus size clothing store",
    "Entertainment agency",
    "Vintage clothing store",
    "Jeans shop",
    "Allergist",
    "Gastrointestinal surgeon",
    "Hand surgeon",
    "Oncologist",
    "Medical Laboratory",
    "Oldsmobile dealer",
    "Free clinic",
    "General practitioner",
    "Foundation",
    "Audiologist",
    "General Contractor",
    "Technical service",
    "Electric utility company",
    "Battery wholesaler",
    "Home automation company",
    "Air conditioning system supplier",
    "Mechanical engineer",
    "Lighting manufacturer",
    "Auto radiator repair service",
    "Children's Furniture Store",
    "Truck accessories store",
    "Pine furniture shop",
    "Religious Seminary",
    "Diesel fuel supplier",
    "Boot Camp",
    "Muay Thai Boxing Gym",
    "Garage door supplier",
    "Agricultural service",
    "Tile store",
    "Computer hardware manufacturer",
    "Life insurance agency",
    "Home insurance agency",
    "Auto insurance agency",
    "Title company",
    "Loss Adjuster",
    "Burglar alarm store",
    "News service",
    "Diamond dealer",
    "Party store",
    "Diamond Buyer",
    "Jewelry Buyer",
    "Hematologist",
    "Occupational medical physician",
    "Orthopedic surgeon",
    "Diabetologist",
    "Fertility physician",
    "Otolaryngology clinic",
    "Industrial equipment supplier",
    "Pediatric Cardiologist",
    "Real Estate Attorney",
    "Patent Attorney",
    "Civil Law Attorney",
    "Tax Attorney",
    "Insurance Attorney",
    "Attorney referral service",
    "Food and beverage consultant",
    "Factory equipment supplier",
    "CD store",
    "Web hosting company",
    "Computer accessories store",
    "Housing complex",
    "Tropical fish store",
    "Aquarium shop",
    "Reptile store",
    "Physiotherapy equipment supplier",
    "Home furnishings",
    "Motel",
    "Falafel restaurant",
    "Malaysian restaurant",
    "Brunch restaurant",
    "Cheesesteak restaurant",
    "Preschool",
    "Business school",
    "English language school",
    "Photography school",
    "Gymnasium Cz",
    "Military school",
    "Metal supplier",
    "Sewing machine store",
    "Air conditioning store",
    "Head start center",
    "Law school",
    "German language school",
    "Handbags shop",
    "Massage Therapist",
    "Fish spa",
    "Health spa",
    "Thai Massage Therapist",
    "Spa Resort",
    "Resort",
    "Facial spa",
    "Sauna",
    "Weight loss service",
    "Fabric store",
    "Aircraft supply store",
    "Electronics",
    "Fish store",
    "Wi-Fi Spot",
    "Fire protection consultant",
    "Cable company",
    "Traffic police station",
    "Palace",
    "Police station",
    "Business development service",
    "Mobile Money Agent",
    "Event management company",
    "Haute couture fashion house",
    "Toy museum",
    "Tower",
    "Repair service",
    "Furniture wholesaler",
    "Bus Charter",
    "Drug store",
    "Wood and laminate flooring supplier",
    "Flooring store",
    "Hookah store",
    "Internet shop",
    "Escape room center",
    "Real Estate Appraiser",
    "Eastern European restaurant",
    "Dairy",
    "Public Sector Bank",
    "Filipino restaurant",
    "Adult education school",
    "School district office",
    "Payroll service",
    "Girls' High School",
    "Shoe repair shop",
    "Tennis store",
    "Medical book store",
    "Private Golf Course",
    "Playground",
    "Fishing club",
    "Hammam",
    "Frozen food store",
    "Souvenir store",
    "Driving school",
    "Car leasing service",
    "Woodworking supply store",
    "Fabric product manufacturer",
    "Textile Mill",
    "Glass & mirror shop",
    "Wheel alignment service",
    "DVD store",
    "Tourist information center",
    "Congregation",
    "Cathedral",
    "Chapel",
    "Registry office",
    "Cultural center",
    "Balloon ride tour agency",
    "Used clothing store",
    "Optometrist",
    "Medical diagnostic imaging center",
    "Optical wholesaler",
    "Call center",
    "Emergency care physician",
    "Urologist",
    "Building materials supplier",
    "Machining manufacturer",
    "Construction equipment supplier",
    "Dry wall supply store",
    "Blacksmith",
    "Iron steel contractor",
    "Pipe supplier",
    "Roofing supply store",
    "Masonry Contractor",
    "Rug store",
    "Insulation materials store",
    "Elevator service",
    "Kitchen Remodeler",
    "Glass shop",
    "Marble contractor",
    "Aluminum supplier",
    "Holding company",
    "Steel Fabricator",
    "Ceramic manufacturer",
    "Used computer store",
    "Gas company",
    "Mercantile development",
    "Private Sector Bank",
    "Digital printing service",
    "Chemical manufacturer",
    "Military hospital",
    "Heart hospital",
    "Goldsmith",
    "Food Court",
    "SCUBA Instructor",
    "Camping farm",
    "Mercedes-Benz dealer",
    "Consultant",
    "General register office",
    "City district office",
    "State Social Development",
    "Medical Examiner",
    "Auto spring shop",
    "Insulation contractor",
    "Hub cap supplier",
    "Recycling center",
    "Juice shop",
    "Pressure washing service",
    "New American restaurant",
    "City Department of Environment",
    "City Clerk's Office",
    "Opera house",
    "Social club",
    "Shrine",
    "Oriental medicine clinic",
    "National park",
    "Nature Preserve",
    "National Reserve",
    "Day care center",
    "Hiking Area",
    "Business park",
    "International Airport",
    "Motoring club",
    "Pediatric Ophthalmologist",
    "Air conditioning contractor",
    "Computer security service",
    "Investment service",
    "Real Estate Fair",
    "Interior fitting contractor",
    "Fitted furniture supplier",
    "Computer desk store",
    "Moroccan restaurant",
    "Nursing school",
    "Child care agency",
    "Ballet school",
    "Swimming school",
    "Montessori school",
    "School house",
    "Pilates studio",
    "Karate school",
    "Oil & Natural Gas Company",
    "Scrapbooking store",
    "Fruit and vegetable store",
    "Thrift store",
    "Oriental rug store",
    "Petroleum products company",
    "Trading card store",
    "Event Ticket Seller",
    "Elder law attorney",
    "Lawyers association",
    "Media company",
    "Food manufacturer",
    "Science museum",
    "Sculpture museum",
    "Local history museum",
    "Modern art museum",
    "Home builder",
    "Paint manufacturer",
    "Paintball center",
    "Industrial Area",
    "Industry",
    "Faculty of Pharmacy",
    "Private University",
    "Acupuncture clinic",
    "Financial Planner",
    "Instruction",
    "Auto Auction",
    "Opel dealer",
    "Renault dealer",
    "Educational supply store",
    "Door shop",
    "School supply store",
    "Paper bag supplier",
    "Beauty products wholesaler",
    "Wallpaper store",
    "Used tire shop",
    "Pumping equipment and service",
    "Yarn store",
    "Haberdashery",
    "Alliance Church",
    "Russian Orthodox Church",
    "Cigar shop",
    "Municipal administration office",
    "Cruise agency",
    "Telecommunications engineer",
    "Distribution service",
    "Lumber store",
    "Metal finisher",
    "Packaging Machinery",
    "Design",
    "Solar hot water system supplier",
    "Ceramics wholesaler",
    "Rainwater tank supplier",
    "Sheet metal contractor",
    "Scaffolding rental service",
    "Metal heat treating service",
    "Pyramid",
    "Tailor",
    "Cosmetics Industry",
    "Psychotherapist",
    "Psychologist",
    "Wholesale bakery",
    "Nursing agency",
    "Pediatric Dentist",
    "Chiropractor",
    "Civil engineer",
    "Light bulb supplier",
    "Wax supplier",
    "Used CD store",
    "Video conferencing equipment supplier",
    "Measuring instruments supplier",
    "Display stand manufacturer",
    "Forklift dealer",
    "Window tinting service",
    "Oil store",
    "Episcopal Church",
    "Formal wear store",
    "Lapidary",
    "Knit shop",
    "Cultural association",
    "Education",
    "Custom confiscated goods store",
    "Marriage Celebrant",
    "Income protection insurance",
    "Municipal corporation",
    "Pet Friendly Accommodation",
    "Grill",
    "Guest house",
    "Event venue",
    "Luxury hotel",
    "Bed & breakfast",
    "Outdoor movie theater",
    "Historical landmark",
    "Media house",
    "Free parking lot",
    "Plateau",
    "Angler fish restaurant",
    "Digital Printer",
    "Meat dish restaurant",
    "Hotel management school",
    "Religious school",
    "School For The Deaf",
    "Public school",
    "Higher secondary school",
    "Mirror shop",
    "Cruise line company",
    "Corporate campus",
    "Window treatment store",
    "Frozen yogurt shop",
    "General store",
    "Kiosk",
    "Civil Registry",
    "Building society",
    "Bicycle wholesale",
    "Traditional Teahouse",
    "Electric motor store",
    "Cabinet store",
    "Door supplier",
    "Towing Equipment Provider",
    "Gift wrap store",
    "Landscaping supply store",
    "Paper store",
    "Acrylic store",
    "Golf cart dealer",
    "Lamination service",
    "Sewing machine repair service",
    "Industrial Supermarket",
    "Porsche dealer",
    "Wedding service",
    "Garden furniture shop",
    "Western apparel store",
    "Finishing materials supplier",
    "Business center",
    "German restaurant",
    "Hearing aid store",
    "Collectibles store",
    "Pest control service",
    "Garden center",
    "Beauty product supplier",
    "Solar energy company",
];
var html = '';

$.each(types, function (index, value) {
    var name = value.replace(/_/g, " ");
    html += '<div><label><input type="checkbox" class="types" value="' + value + '" />' + name + '</label></div>';
});

$('#type_holder').html(html);

var url = '';
var selectedItems = "";
function getValues() {

    selectedTypes = [];
    $('.types').each(function () {
        if ($(this).is(':checked')) {
            selectedTypes.push($(this).val());
        }
    });

    console.log(selectedTypes.length)

    var selectedItems = "";
    for (var i = 0; i < selectedTypes.length; i++) {

        selectedItems += "'" + selectedTypes[i] + "',";

    }
    selectedItems = selectedItems.substring(0, selectedItems.length - 1);
    console.log(selectedItems);

    map.setLayoutProperty('pointlayer', 'visibility', 'none');
    map.removeLayer('pointlayer');
    map.removeSource('tbl_data')
    var url1 = geoserverlink + "/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=" + layername + "&outputFormat=application%2Fjson&CQL_FILTER=Type%20in%20(" + selectedItems + ")";
    console.log(url1);

    map.addSource('tbl_data', {
        type: 'geojson',
        data: url1
    });

    map.addLayer({
        'id': 'pointlayer',
        'type': 'circle',
        'source': 'tbl_data',
        'layout': {},
        'paint': {
            "circle-radius": 8,
            "circle-color": ['get', 'color']
        }
    });

}


let needofpitch = true;

function pitch() {
    if (needofpitch == true) {
        map.setPitch(60, 1);
        needofpitch = false;
    } else {
        map.setPitch(0, 1);
        needofpitch = true;
    }
}

function myFunction() {
    if ($('#forisochronelegend')[0].style.display == 'none') {
        $('#forisochronelegend')[0].style.display = 'block'
    } else {
        $('#forisochronelegend')[0].style.display = 'none'
    }
    // console.log("isochrone");
    const x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }

}

var colorSchemes = {
    mono: [
        [0, 'lime'],
        [300, 'green'],
        [600, 'blue'],
        [900, 'purple']
    ],
    rainbow: [
        [0, 'red'],
        [150, 'orange'],
        [300, 'yellow'],
        [450, 'green'],
        [600, 'blue'],
        [750, 'indigo'],
        [900, 'violet'],


    ],
    altColor: [
        [0, '#f54e5e'],
        [600, '#f9886c'],
        [1200, '#f1f075'],
        [1800, '#56b881'],
        [2500, '#3887be'],
        [3600, '#4a4a8b']
    ],

    pendleton: [
        [150, '#eae49a'],
        [300, '#e3ce4f'],
        [600, '#eeab50'],
        [900, '#ec8353'],
        [1200, '#c88e9c'],
        [1500, '#b0517d'],
        [1800, '#375b97']
    ]
};

var ddsColor =
{
    "property": "time",
    "type": 'exponential',
    "stops": colorSchemes.altColor
};


var nothing =
{
    "type": "geojson",
    "data": {
        "type": "FeatureCollection",
        "features": []
    }
};

var hulls;

function visualize(geom) {
    geom.features = geom.features.map(function (ft) {
        const modified = ft;
        const seconds = ft.properties.time;
        modified.properties.minutes = seconds / 60;
        modified.properties.quantized = seconds % 600 === 0 ? 3600 : (seconds % 300 === 0 ? 1800 : (seconds % 300 === 0 ? 900 : 1));
        modified.properties.area = ruler.area(modified.geometry.coordinates);
        return modified
    }).reverse();
    map.getSource('hulls').setData(geom);
    map.setPaintProperty('continuous', 'fill-opacity', 1);
    map.on('mousemove', setHighlight);
    map.on('touchmove', setHighlight);

    function setHighlight(e) {

        const ft = map.queryRenderedFeatures(e.point, { layers: ['continuous'] })[0];
        const time = ft ? ft.properties.time : 'nonehighlighted';

        const noTooltip = !state.inspector || !ft;

        if (!noTooltip) {
            //update tooltip position
            const tooltip = document.querySelector('#tooltip');
            tooltip.style.transform = 'translateX(' + e.point.x + 'px) translateY(' + e.point.y + 'px)';

            const polygon = geom.features.filter(function (feature) {
                return feature.properties.time === time
            });

            document.querySelector('#time').innerHTML = secondsToMinutes(time);
            document.querySelector('#area').innerHTML = parseInt(polygon[0].properties.area) + ' mi';

            map.getSource('highlight').setData(turf.featureCollection(polygon))
        }

        // toggle highlight visibility
        const highlightVisibility = !noTooltip && ft ? 1 : 0;
        const inspectorVisibility = !noTooltip && ft ? 'inspector' : '';

        map.setPaintProperty('highlight', 'fill-opacity', highlightVisibility);
        document.querySelector('#map').classList = inspectorVisibility
    }

    d3.select('#loader')
        .classed('hidden', true);
    fitBounds(state.threshold, geom);
}


var state = {
    token: 'pk.eyJ1IjoicGV0ZXJxbGl1IiwiYSI6ImNqdHJqdG92OTBkNTg0M3BsNDY0d3NudWcifQ.0z4ov_viFE-yBMSijQpOhQ',
    debug: window.location.search.indexOf('debug') > -1,
    mode: 'driving',
    direction: 'divergent',
    threshold: 3600,
    resolution: 1.5,
    batchSize: 400,
    inspector: false
};

function setState(parameter, value) {
    // console.log(parameter);
    // console.log(value);
    // console.log("setState");
    if (parameter == 'inspector') {
        state[parameter] = JSON.parse(value);
    } else {
        state[parameter] = value;
    }

    //changing mode of transport
    // if (parameter === 'mode'){
    // var icons = {driving: 'car', cycling: 'bike', walking: 'walk'};
    // document.querySelectorAll('use')[0].setAttribute('xlink:href','#icon-'+icons[value])
    // }
    if (parameter === 'threshold') {
        const limit = parseInt(state.threshold);
        state.threshold = limit;
        d3.select('#max').text(limit / 60);
        const filter = ['all', ['>=', 'quantized', limit], ['<=', 'time', limit]];
        map.setFilter('quantized-major', filter);
        map.setFilter('quantized-label', filter);
        map.setFilter('hulls', filter[2]);
        adjustColorRamp(limit);
    }

    if (parameter === 'color') {
        // console.log("color", value);
        const layers = ['continuous', 'quantized', 'quantized-label', 'quantized-major'];

        layers.forEach(function (layer) {
            const visibility = layer.includes(value) ? 'visible' : 'none';

            map.setLayoutProperty(layer, 'visibility', visibility)
        });
        return
    }


    clearMap();

}

function createisochrone() {
    $("body").css("cursor", "progress");
    isochrone(c, state, visualize);


    d3.select('#loader')
        .classed('hidden', false)
}

function visualize(geom) {
    geom.features = geom.features.map(function (ft) {
        const modified = ft;
        const seconds = ft.properties.time;
        modified.properties.minutes = seconds / 60;
        modified.properties.quantized = seconds % 600 === 0 ? 3600 : (seconds % 300 === 0 ? 1800 : (seconds % 300 === 0 ? 900 : 1));
        modified.properties.area = ruler.area(modified.geometry.coordinates);
        return modified
    }).reverse();
    map.getSource('hulls').setData(geom);
    map.setPaintProperty('continuous', 'fill-opacity', 1);
    map.on('mousemove', setHighlight);
    map.on('touchmove', setHighlight);

    function setHighlight(e) {

        const ft = map.queryRenderedFeatures(e.point, { layers: ['continuous'] })[0];
        const time = ft ? ft.properties.time : 'nonehighlighted';

        const noTooltip = !state.inspector || !ft;

        if (!noTooltip) {
            //update tooltip position
            const tooltip = document.querySelector('#tooltip');
            tooltip.style.transform = 'translateX(' + e.point.x + 'px) translateY(' + e.point.y + 'px)';

            const polygon = geom.features.filter(function (feature) {
                return feature.properties.time === time
            });

            document.querySelector('#time').innerHTML = secondsToMinutes(time);
            document.querySelector('#area').innerHTML = parseInt(polygon[0].properties.area) + ' mi';

            map.getSource('highlight').setData(turf.featureCollection(polygon))
        }

        // toggle highlight visibility
        const highlightVisibility = !noTooltip && ft ? 1 : 0;
        const inspectorVisibility = !noTooltip && ft ? 'inspector' : '';

        map.setPaintProperty('highlight', 'fill-opacity', highlightVisibility);
        document.querySelector('#map').classList = inspectorVisibility
    }

    d3.select('#loader')
        .classed('hidden', true);
    fitBounds(state.threshold, geom);
    $("body").css("cursor", "default");
}

var c = map.getCenter();
c = [c.lng, c.lat];
const el = createIcon('car');
// el.className = 'origin icon';
// // add marker to map
var marker = new mapboxgl.Marker(el);
//     .setLngLat(c)
//     .addTo(map);

// marker.setLngLat(c);
// document.querySelectorAll('use')[0].setAttribute('xlink:href','#icon-car');

function createIcon(icon) {
    const svgNS = 'http://www.w3.org/2000/svg';
    const xlinkNS = 'http://www.w3.org/1999/xlink';
    const svg = document.createElementNS(svgNS, 'svg');
    svg.setAttributeNS(null, 'class', 'icon origin');
    const use = document.createElementNS(svgNS, 'use');
    use.setAttributeNS(xlinkNS, 'xlink:href', '#icon-' + icon);
    svg.appendChild(use);
    return svg;
}

function adjustColorRamp(threshold) {
    const newColor = {
        "property": "time",
        "type": 'exponential',
    };
    const factor = threshold / 3600;
    const ramp = ddsColor.stops.map(function (item) {
        return [item[0] * factor, item[1]]
    });
    newColor.stops = ramp;
    map.setPaintProperty('continuous', 'fill-color', newColor);
    map.setPaintProperty('quantized', 'line-color', newColor);
    map.setPaintProperty('quantized-major', 'line-color', newColor);
    map.setPaintProperty('quantized-label', 'text-color', newColor)


}

function clearMap() {
    map.setPaintProperty('continuous', 'fill-opacity', 0);
    map.getSource('hulls').setData(nothing.data)
}

function secondsToMinutes(seconds) {
    const min = Math.floor(seconds / 60);
    const secRemainder = '00';
    return min + ':' + secRemainder;
}

function fitBounds(threshold, geom) {
    const poly = geom.features.filter(function (item) {
        return item.properties.time === threshold
    });
    const bbox = (turf.bbox(turf.featureCollection(poly)));
    map.fitBounds(bbox, { padding: 20 })
}


map.on("moveend", function () {
    const zoom = map.getZoom();
    const lat = map.getCenter().lat;
    const lng = map.getCenter().lng;
    sessionStorage.setItem("zoom", zoom);
    sessionStorage.setItem("lat", lat);
    sessionStorage.setItem("lat", lng);

});



var uniqueArray = []
function getSuggestionValues(val) {
    document.getElementById('inputofquery').value = ''
    cqlfilter = ''
    var allselectedtypes = $('.Typefromgeoserver')
    var availabletypes = []
    if (allselectedtypes.length != 0) {

        for (j = 0; j < allselectedtypes.length; j++) {
            availabletypes.push(allselectedtypes[j].id)

        }
    }
        $.getJSON(geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&PROPERTYNAME=Type,' + val.value + '&outputformat=application/json', function (data) {
            console.log(data)
            var allfeat = data.features
            uniqueArray = []
            for (i = 0; i < allfeat.length; i++) {
                if (availabletypes.includes(allfeat[i].properties["Type"])) {
                    var columnval = allfeat[i].properties[val.value]
                    if (uniqueArray.includes(columnval)) {

                    } else {
                        uniqueArray.push(columnval)
                    }
                }
            }
            autocomplete(document.getElementById("inputofquery"), uniqueArray)
        })
    }

    $.getJSON(geoserverlink + '/ows?service=WFS&version=1.0.0&request=describeFeatureType&typeName=' + layername + '&outputformat=application/json', function (data) {
        console.log(data)
        var noneedcolumns = ['the_geom', 'GIS_ID', 'id', 'Lat', 'Lng']
        var select = document.getElementById('columnofquery')
        var allcolumns = data.featureTypes[0].properties
        for (i = 0; i < allcolumns.length; i++) {
            if (noneedcolumns.includes(allcolumns[i].name)) {

            } else {
                var op = document.createElement('option')
                op.value = allcolumns[i].name
                op.innerHTML = allcolumns[i].name
                select.appendChild(op)
            }
        }
    })

    function queryresult() {
        var columnname = document.getElementById('columnofquery').value
        var value = document.getElementById('inputofquery').value
        var querylink = geoserverlink + "/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=" + layername + "&outputFormat=application%2Fjson&CQL_FILTER=" + columnname + "='" + value + "'"
        if (map.getSource('query_data')) {
            map.getSource('query_data').setData(querylink)
        } else {
            map.addSource('query_data', {
                type: 'geojson',
                data: querylink
            });
            map.addLayer({
                'id': 'queryLayer',
                'type': 'circle',
                'source': 'query_data',
                'layout': {},
                'paint': {
                    'circle-radius': 7,
                    'circle-color': '#223b53',
                    'circle-stroke-color': 'white',
                    'circle-stroke-width': 3,
                    'circle-opacity': 0.1
                }
            })
        }
        $.getJSON(querylink, function (data) {
            console.log(data)
            var bounds = new mapboxgl.LngLatBounds();

            var allfeat = data.features
            if (allfeat.length == 0) {
                alert('no data found in query')
            } else {
                for (j = 0; j < allfeat.length; j++) {
                    bounds.extend(allfeat[j].geometry.coordinates);

                }
                map.fitBounds(bounds);
            }

            //adding data to datatable
            if ($.fn.DataTable.isDataTable('#datatbl')) {
                $('#datatbl').DataTable().destroy();
                $('#datatbl tbody').empty();
            }
            var allhead
            var allbody = []
            allhead = Object.keys(allfeat[0].properties)
            for (f = 0; f < allfeat.length; f++) {
                allbody.push(Object.values(allfeat[f].properties))
            }
            
    var coumnss = []
    for (v = 0; v < allhead.length; v++) {
        var tempobj = {}
        tempobj.title = allhead[v]
        coumnss.push(tempobj)
    }
    $('#datatbl').DataTable({
        data: allbody,
        columns: coumnss,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
        })
        flagtablefromatrrquery = true
    }

    function clearqueryresult() {
        map.removeLayer('queryLayer')
        map.removeSource('query_data')
        if (flagtablefromatrrquery = true){
            if ($.fn.DataTable.isDataTable('#datatbl')) {
                $('#datatbl').DataTable().destroy();
                $('#datatbl tbody').empty();
            }
        }
        flagtablefromatrrquery = false
    }
    //// auto complete
    function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function (e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) { return false; }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.style.color = 'black'
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase().trim() == val.toUpperCase().trim()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function (e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function (e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });
        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }
        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });
    }


    // side bar query 
    function sideQuery() {
        var els = document.getElementsByClassName('Typefromgeoserver');
        for (var i=0;i<els.length;i++){
            var li = els[i].nextSibling.nextSibling.nextSibling.children;
            if ( els[i].checked ) {
                for(var j=0;j<li.length;j++) {
                    li[j].children[0].checked = true
                }
            } else {
                for(var j=0;j<li.length;j++) {
                    li[j].children[0].checked = false
                }
            }
        }
        var allselectedtypes = $('.Typefromgeoserver:checkbox:checked')
        var allselectedcategorie = $('.Categoryfromgeoserver:checkbox:checked')
        var cqlfilter = '1 = 1 '
        var Typefilter = ''
        var Catfilter = ''
        if (allselectedtypes.length != 0) {
            Typefilter = 'Type in ('
            for (j = 0; j < allselectedtypes.length; j++) {
                if (j == 0) {
                    Typefilter += "'" + allselectedtypes[j].id + "'"
                } else {
                    Typefilter += ",'" + allselectedtypes[j].id + "'"
                }
            }
            Typefilter += ")"
        }
        if (allselectedcategorie.length != 0) {
            Catfilter = 'Category in ('
            for (j = 0; j < allselectedcategorie.length; j++) {
                if (j == 0) {
                    Catfilter += "'" + allselectedcategorie[j].id + "'"
                } else {
                    Catfilter += ",'" + allselectedcategorie[j].id + "'"
                }
            }
            Catfilter += ")"
        }

        if (Catfilter != '' && Typefilter != '') {
            cqlfilter = Catfilter + ' or ' + Typefilter
        } else if (Catfilter != '') {
            cqlfilter = Catfilter
        } else if (Typefilter != '') {
            cqlfilter = Typefilter
        } else if (Catfilter == '' && Typefilter == '') {
            cqlfilter = "Type = 'dxv'"
        }
        var geom = geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&outputFormat=application%2Fjson&CQL_FILTER=' + cqlfilter

        map.getSource('tbl_data').setData(geom);
        if (allselectedtypes.length > 1) {
            map.setPaintProperty('pointlayer', 'circle-color', basedontype);
        } else {
            map.setPaintProperty('pointlayer', 'circle-color', basedoncat);

        }
    }


    //toggle alllayers 
    function Alllayertoggle() {

        if (document.getElementById('togglealllayers').checked == true) {
            var allselectedtypes = $('.Typefromgeoserver:checkbox')

            for (k = 0; k < allselectedtypes.length; k++) {
                allselectedtypes[k].checked = true
            }

            sideQuery()

            
        
            var allselectedtypes = $('.Typefrombasemap:checkbox')

            for (k = 0; k < allselectedtypes.length; k++) {
                allselectedtypes[k].checked = true
                showmapboxlayer(allselectedtypes[k])
            }
            

        } else {
            var allselectedtypes = $('.Typefromgeoserver:checkbox:checked')
            var allselectedcategorie = $('.Categoryfromgeoserver:checkbox:checked')

            var allselectedtypes = $('.Typefromgeoserver:checkbox:checked')
            var allselectedcategorie = $('.Categoryfromgeoserver:checkbox:checked')
            for (k = 0; k < allselectedtypes.length; k++) {
                allselectedtypes[k].checked = false
            }
            for (k = 0; k < allselectedcategorie.length; k++) {
                allselectedcategorie[k].checked = false
            }

            var geom = geoserverlink + "/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=" + layername + "&outputFormat=application%2Fjson&CQL_FILTER=Type='dzvxv'"

            map.getSource('tbl_data').setData(geom);
            var allselectedtypes = $('.Typefrombasemap:checkbox')

            for (k = 0; k < allselectedtypes.length; k++) {
                allselectedtypes[k].checked = false
                showmapboxlayer(allselectedtypes[k])
            }
        }

    };

    document.getElementById('allocatedLayers').innerText.split(",")
    //show all usersinfo 
    // $.get('read',

    //     function (readResult) {
    //         $.getJSON(geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&PROPERTYNAME=Type&outputformat=application/json', function (Alltypes) {

    //             var allfeat = Alltypes.features
    //             var alltypnamesarray = []
    //             for (i = 0; i < allfeat.length; i++) {
    //                 var columnval = allfeat[i].properties["Type"]
    //                 if (alltypnamesarray.includes(columnval)) {

    //                 } else {
    //                     alltypnamesarray.push(columnval)
    //                 }
    //             }
    //             var tpeselect = document.createElement('select')
    //             for (p = 0; p < alltypnamesarray.length; p++) {
    //                 var op = document.createElement('option')
    //                 op.value = alltypnamesarray[p]
    //                 op.innerHTML = alltypnamesarray[p]
    //                 tpeselect.appendChild(op)
    //             }

    //             var theBody = document.getElementById('usermanagementtablebody')
    //             var allres = readResult.result
    //             for (i = 0; i < allres.length; i++) {
    //                 var row = document.createElement('tr')
    //                 var cell = document.createElement('td')
    //                 cell.innerHTML = allres[i].name
    //                 row.appendChild(cell)
    //                 var cell = document.createElement('td')

    //                 cell.innerHTML = allres[i].email
    //                 row.appendChild(cell)
    //                 var cell = document.createElement('td')
    //                 cell.appendChild(tpeselect.cloneNode(true))
    //                 row.appendChild(cell)
    //                 var cell = document.createElement('td')

    //                 cell.innerHTML = '<select> <option value="">Select Role</option> <option value="Admin">Admin</option><option value="User">User</option></select>'




    //                 row.appendChild(cell)
    //                 var button = document.createElement('button')
    //                 button.innerText = 'update'
    //                 button.addEventListener('click', function () { updatequery(this) })
    //                 row.appendChild(button)

    //                 theBody.appendChild(row)
    //             }
    //         })
    //     })


    function displayusermanagementtable() {
        var table = document.getElementById('usermanagementtable')
        if (table.style.display == 'none' || table.style.display == "") {
            table.style.display = 'block'
        } else {
            table.style.display = 'none'
        }
    }



    function updatequery(loc) {

        var role = loc.parentElement.getElementsByTagName('td')[3].getElementsByTagName('input')[0].value
        console.log(role)
        var cat = loc.parentElement.getElementsByTagName('td')[2].getElementsByTagName('input')[0].value
        console.log(cat)
    }


    //save edited user management stuff
    function user_management_saving() {
        $.ajax({
            url: "{{URL::action('DataController@post_appajax')}}",
            type: "POST",
            data: { action: 'refresh' }
        })
            .done(function (json) {
                console.log(json);
            });


    }

    $.getJSON(geoserverlink + '/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=' + layername + '&PROPERTYNAME=Type&outputformat=application/json', function (Alltypes) {

        var allfeat = Alltypes.features
        var alltypnamesarray = []
        for (i = 0; i < allfeat.length; i++) {
            var columnval = allfeat[i].properties["Type"]
            if (alltypnamesarray.includes(columnval)) {

            } else {
                alltypnamesarray.push(columnval)
            }
        }
        var tpeselect = document.createElement('select')
        tpeselect.id = 'allselectedtypes'
        tpeselect.multiple = true
        tpeselect.className = 'form-control'
        tpeselect.onchange = function () {
            document.getElementById('catofuser').value = $('#allselectedtypes').val().toString()

        }
        for (p = 0; p < alltypnamesarray.length; p++) {
            var op = document.createElement('option')
            op.value = alltypnamesarray[p]
            op.innerHTML = alltypnamesarray[p]
            tpeselect.appendChild(op)
        }
        document.getElementById('typeselectors').appendChild(tpeselect)
    })
    $('.openeditcard').on('click', function () {
        $( "#editdataformdiv" ).toggleClass( "d-none" )
        $( "#usermanagementtablebody" ).toggleClass( "d-none" )

        $tr = $(this).closest('tr')

        var data = $tr.children('td').map(function () {
            return $(this).text();
        }).get();

        document.getElementById('idofuser').value = data[0]
        document.getElementById('nameofuser').innerText = data[1]
    })

    $('#goBack').on('click', function(e){
        e.preventDefault();
        $( "#editdataformdiv" ).toggleClass( "d-none" )
        $( "#usermanagementtablebody" ).toggleClass( "d-none" )
    })


    $('#editdataform').on('submit', function (e) {
        e.preventDefault();

        var id = parseInt($('#idofuser').val())

        $.ajax({
            type: "PUT",
            url: '/userupdate/' + id,
            data: $('#editdataform').serialize(),
            success: function (response) {
                alert('updated')
                document.getElementById('editdataformdiv').style.display = 'none'
            },
            error: function (err) {
                alert(err)
            }
        })
    })

    function updateinput() {
        document.getElementById('roleofuser').value = document.getElementById('roleofusersel').value
    }


    // data tables 
    $(document).ready(function () {
        $('#userManagement').click(function(e) {
            e.preventDefault();
            $('#usermanagementtable').dialog('open');
        });
        $('#usermanagementtable').dialog({
            autoOpen: false,
            width: "90%",
            minHeight: "98%",
            position: {
                my: "center top",
                at: "center top",
                of: window
            },
            show: {
                effect: "fade",
                duration: 0
            },
            hide: {
                effect: "fade",
                duration: 0
            },
            open: function (type, data) {
                $('#usermanagementtablebody').DataTable();
            }
        });
        $('.openDT').on('click', function(e) {
            e.preventDefault();
            $(".DTsec").removeAttr("style");
            $(".DTsec").toggleClass("show");
        })
        $('.DTsec').resizable({
            handles: {
                'n': '#handle'
            }
        });
    });

    function turnoffusermanagement() {
        document.getElementById('usermanagementtable').style.display = 'none'
    }


    // function to show query 
    function myFunctionforquery(){
        if ($('#myqueryDIVattr')[0].style.display == 'none') {
            $('#myqueryDIVattr')[0].style.display = 'block'
        } else {
            $('#myqueryDIVattr')[0].style.display = 'none'
        }
    }