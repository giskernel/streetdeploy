@extends('layouts.app')

@section('header_scripts')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.0.0/mapbox-gl-geocoder.css' type='text/css' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css' type='text/css' />
<link rel='stylesheet' href='https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css' type='text/css' />
<link rel='stylesheet' href='https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css' type='text/css' />

@endsection

@section('content')
{{-- start map --}}
<div id="wrapper">
  <div id="map">
    <div id='tooltip' class='dark'>
      <div class='dot'></div>
      <div class='line'></div>
      <div class='bubble'>
        <span class="txt-bold txt-h2" id='time'></span>
        <span class="quiet txt-s txt-light">
          (<span id='area'></span><sup class='txt-sup'>2</sup>)
        </span>
      </div>
    </div>

  </div>

{{-- 
  <div class="content" id='smartsearch'>
    <select id='columnofquery' onchange='getSuggestionValues(this)'>
      <option> Select Column name</option>
    </select>
    <input type="text" name="" id="inputofquery" placeholder="Search">
    <button class="btn btn-secondary btn-sm" id='querytheresult' onclick="clearqueryresult()"><i class="icofont-close-line"></i></button>
    <button class="btn btn-dark btn-sm" id='querytheresult' onclick="queryresult()">Query</button>
  </div> --}}
</div>

<div class="DTsec">
  <div id="handle" class="ui-resizable-handle ui-resizable-n"></div>
  <span class="openDT">Show Data Table</span>
  <div class="DTwrapper">
    <table id="datatbl" class="display"></table>
  </div>
</div>
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> -->
<div id='usermanagementtable' class="popup">
  <!-- <i class="icofont-close-squared" onclick='turnoffusermanagement()'></i> -->
  <table id='usermanagementtablebody'>
    <thead>
      <tr>
        <td>Id </td>
        <td>Name</td>
        <td>Email</td>
        <td>Categories</td>
        <td>Role</td>
        <td>Action</td>
      </tr>
    </thead>
    <!-- <tbody id='usermanagementtablebody'> -->
    <tbody >
      @foreach ($users as $user)
      <tr>
        <td> {{ $user->id }} </td>
        <td> {{ $user->name }} </td>
        <td> {{ $user->email }} </td>
        <td> {{ $user->role }} </td>
        <td> {{ $user->categories }} </td>
        <td class="text-center"> 
          <button class='openeditcard'><i class="icofont-edit"></i></button>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  <div id='editdataformdiv' class="d-none"> 
    <form id='editdataform'>
      {{ csrf_field() }}
      {{ method_field('PUT') }}
      <input type='hidden' name='idofuser' id='idofuser'>
      <table>
        <tr>
          <th>Account User:</th>
          <td><span id='nameofuser'></span></td>
        </tr>
        <tr>
          <th>Type selector:</th>
          <td><span id='typeselectors'></span></td>
        </tr>
        <tr>
          <th>Role selector:</th>
          <td>
            <select onchange='updateinput()' class="form-control" id='roleofusersel' > 
              <option > Select role type </option> 
              <option value='admin'> Admin </option>
              <option value='user'> User </option>
            </select>
          </td>
        </tr>
        <input style='display:none' name='catofuser' id='catofuser'>
        <input  style='display:none'  name='roleofuser' id='roleofuser'>
        <tr>
          <td colspan="2" class="text-right">
            <button class="btn btn-sm btn-secondary" id="goBack"> Go Back </button>
            <button type='submit' class="btn btn-sm btn-primary"> Update </button>
          </td>
        </tr>
      </table>
    </form>
  </div>
</div>
<!--<nav id="menu">

 <div class="dropdown ab" id='opencls'>
 
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Dropdown Example
  <span class="caret"></span></button>
 
  <ul class="dropdown-menu " id="layerz">

  </ul>
</div>

</nav> -->
<a href="#" onclick="home()" class="btn-home"><i class="icofont-ui-home"></i></a>
<a href="#" onclick="pitch()" class="pitch-home"><i class="icofont-cube"></i></a>
<a href="#" onclick="myFunction()" class="isochorne-home"><i class="icofont-map"></i></a>
<a href="#" id='smartsearch' onclick="myFunctionforquery()" class="query-home"><i class="icofont-search"></i></a>
<div class="forblocklegend" id="forblocklegend" style='display:none'></div>
<div class="forisochrone container-fluid" id="myDIV" style='display:none'>
  <div class='p18'>
    <span class='txt-l'>Total area that's</span>
    <!-- <div class='select-container'> -->
    <select class='select select--s select--stroke select--stroke-white pt3 pb3'
      onchange='setState("direction", this.value)'>
      <option value='divergent'>reachable from</option>
      <option value='convergent'>able to reach</option>
    </select>

    <!-- <div class='select-arrow'></div> -->
    <!-- </div> -->
    <span class='txt-l'>this point, within a </span>
    <!-- <div class='select-container'> -->
    <select class='select select--s select--stroke select--stroke-white pt3 pb3'
      onchange='setState("threshold", this.value)'>
      <option value='3600'>60-minute</option>
      <option value='1800'>30-minute</option>
      <option value='900'>15-minute</option>
      <option value='600'>10-minute</option>
      <option value='300'>5-minute</option>
    </select>
    <!-- <div class='select-arrow'></div> -->
    <!-- </div> -->
    <!-- <div class='select-container'> -->
    <select class='select select--s select--stroke select--stroke-white pt3 pb3'
      onchange='setState("mode", this.value)'>
      <option value='driving'>drive</option>
      <option value='cycling'>bike ride</option>
      <option value='walking'>walk</option>
    </select>
    <!-- <div class='select-arrow'></div> -->
    <!-- </div> -->
  </div>
  <hr>
  <div class='px18 py12 border-t border--gray clearfix'>
    <div class="row">
      <div class="col">
        INSPECTOR
      </div>
      <div class="col-auto">
        <select class='select select--s select--stroke select--stroke-white pt3 pb3'
          onchange='setState("inspector", this.value)'>
          <option value=true>ON</option>
          <option selected value=false>OFF</option>
        </select>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col">Type of catchment</div>
      <div class="col-auto">
        <select class='select select--s select--stroke select--stroke-white pt3 pb3'
          onchange='setState("color", this.value)'>
          <option value='quantized'>Quantized</option>
          <option selected value='continuous'>Continuous</option>
        </select>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-auto">SAMPLE RESOLUTION</div>
      <div class="col">
        <input type='range' class="w-100" value='20' min='15' max='70' step='1'
          onchange='setState("resolution", 30/this.value)' />
      </div>
    </div>
    <hr>
    <!-- <div class='toggle-group txt-s fr'>
      <label class='toggle-container'>
        <input name='inspector' type='radio' />
        <div class='toggle toggle--white' onclick='state.inspector=true'>ON</div>
      </label>
      <label class='toggle-container'>
        <input checked name='inspector' type='radio' />
        <div class='toggle toggle--white' onclick='state.inspector=false'>OFF</div>
      </label>
    </div> -->


  </div>
  <!-- <div class='select-container'> -->

  <!-- <div class='select-arrow'></div> -->



  <!-- </select> -->
  Select the point on map and click the button
  <button class="btn btn-primary btn-sm" onclick='createisochrone()'>Create Catchment area</button>

</div>

<div class="forattrquery container-fluid" id="myqueryDIVattr" style='display:none'>
  <p> Please select the Column</p>
  <select id="columnofquery" onchange="getSuggestionValues(this)">
    <option> Select Column name</option>
 </select>
 <hr>
 <p> Please select value</p>
 <input type="text" name="" id="inputofquery" placeholder="Search">
 <hr>
 <button class="btn btn-secondary btn-sm" id='querytheresult' onclick="clearqueryresult()"><i class="icofont-close-line"></i></button>
 <button class="btn btn-dark btn-sm" id='querytheresult' onclick="queryresult()">Query</button>

</div>
<!-- <div class='fl txt-s'>COLOR</div>
    <div class='toggle-group txt-s fr'>
      <label class='toggle-container'>
        <input name='toggle' type='radio' />
        <div class='toggle toggle--white' onclick='setState("color", "quantized") hi()'>Quantized</div>
      </label>
      <label class='toggle-container'>
        <input checked name='toggle' type='radio' />
        <div class='toggle toggle--white' onclick='setState("color", "continuous")'>Continuous</div>
      </label>
    </div>
  </div> -->


<!-- <div class='select-container' id = "myDIV" style="display: none;">
              
              
  </div> -->
<!-- <div id='sidebar' class='m12 bg-darken75 top left round border border--gray color-gray-light'>
       
             -->

<div id="forisochronelegend" style='display:none' class="bg-darken75 m12">
  <div id="band"></div>
  <div class="dark">
    <span style="left:0px; position:absolute; text-align:center;color:white">0</span>
    <span style="left:45%; position:absolute;color:white">MIN</span>
    <span style="right:0px; position:absolute;color:white" id="max">60</span>
  </div>
</div>

<div class="side_panel" id='opencls'>
  <span class="side_panel_opener"><i class="icofont-layers"></i></span>
  <div class="layerlist">

    <h6 style='text-align:center'>Data for the year 2019 in </h6>
    <h3 style='color:blue;text-align:center'>Greater Cairo</h3>
    <button onclick='getdetails()' style='color:green;font-size: smaller;'>Report for current view</button>
    <div class="layerzWrap">
      <ul id="layerz"></ul>
    </div>

    <div class='atbottom'>
      <span class="toggleExpand"><i class="icofont-expand-alt"></i></span>
    </div>
  </div>
  <div class="legend">
    <h4>Legend</h4>
    <div id='legends' class="scroll"></div>
  </div>
</div>



@endsection

@section('footer_scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.0.0/mapbox-gl-geocoder.min.js'></script>
<script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js'></script>
<script src='https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.0/js/buttons.flash.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js'></script>
<script src='https://cdn.datatables.net/buttons/1.6.0/js/buttons.print.min.js'></script>
<!-- <script src="{{ asset('js/app1.js') }}"></script> -->
<script src="{{ asset('js/Javasc.js') }}"></script>
<script src="{{ asset('js/mapb.js') }}"></script>

<script>
  jQuery(".side_panel_opener").click(function () {
    jQuery(".side_panel").toggleClass("show");
  });
  jQuery(".toggleExpand").click(function () {
    jQuery(".side_panel").toggleClass("expand");
  });
</script>
@endsection